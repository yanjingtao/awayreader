unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BusinessSkinForm, bsSkinData, ExtCtrls, bsSkinCtrls, ImgList, Menus,
  bsSkinMenus, bsSkinShellCtrls, bsSkinExCtrls, bsSkinHint, bsSkinBoxCtrls,
  AppEvnts, StdCtrls, Mask, DB, ASGSQLite3, jpeg, GIFImg, bsMessages,
  ReadText, bsTrayIcon, bsDialogs, OleServer, SpeechLib_TLB, IniFiles, ActnList,
  VBScript_RegExp_55_TLB, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, ShlObj, ActiveX;

type
  TReader = class(TForm)
    bsResourceStrData1: TbsResourceStrData;
    bsSkinData1: TbsSkinData;
    bsSkinStatusBar1: TbsSkinStatusBar;
    ImageList1: TImageList;
    HistoryList: TbsSkinPopupMenu;
    RMarkList: TbsSkinPopupMenu;
    R11: TMenuItem;
    R21: TMenuItem;
    R31: TMenuItem;
    R41: TMenuItem;
    R51: TMenuItem;
    SMarkList: TbsSkinPopupMenu;
    S11: TMenuItem;
    S21: TMenuItem;
    S31: TMenuItem;
    S41: TMenuItem;
    S51: TMenuItem;
    H1: TMenuItem;
    H2: TMenuItem;
    H3: TMenuItem;
    H4: TMenuItem;
    H5: TMenuItem;
    OpenDialog1: TbsSkinOpenDialog;
    bsSkinHint1: TbsSkinHint;
    process2: TbsSkinLabel;
    ApplicationEvents1: TApplicationEvents;
    bsSkinEdit1: TbsSkinEdit;
    status: TbsSkinStatusPanel;
    DB1: TASQLite3DB;
    Query1: TASQLite3Query;
    RMessage: TbsSkinMessage;
    Memory1: TTimer;
    AutoMark1: TTimer;
    ClearStatus: TTimer;
    RunTime: TTimer;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    MainPop: TbsSkinPopupMenu;
    SearchButton: TMenuItem;
    CopyButton: TMenuItem;
    MarkButton: TMenuItem;
    N6: TMenuItem;
    FS: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    bsTrayIcon1: TbsTrayIcon;
    Traypop: TbsSkinPopupMenu;
    ShowHide: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N4: TMenuItem;
    bsCompressedStoredSkin1: TbsCompressedStoredSkin;
    OD: TbsOpenSkinDialog;
    N5: TMenuItem;
    Read: TReadText;
    SpVoice1: TSpVoice;
    N9: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    O1: TMenuItem;
    MainScrollbar: TbsSkinScrollBar;
    ToolBar: TbsSkinToolBar;
    bsSkinMenuSpeedButton1: TbsSkinMenuSpeedButton;
    oms: TbsSkinMenuSpeedButton;
    bsSkinBevel1: TbsSkinBevel;
    sms: TbsSkinMenuSpeedButton;
    bsSkinBevel2: TbsSkinBevel;
    bsSkinSpeedButton1: TbsSkinSpeedButton;
    bsSkinSpeedButton2: TbsSkinSpeedButton;
    bsSkinBevel3: TbsSkinBevel;
    bsSkinSpeedButton3: TbsSkinSpeedButton;
    MenuButton: TbsSkinSpeedButton;
    S1: TMenuItem;
    process1: TbsSkinScrollBar;
    N14: TMenuItem;
    N15: TMenuItem;
    ActionList1: TActionList;
    OpenFile1: TAction;
    FullScreen1: TAction;
    Find1: TAction;
    Copy1: TAction;
    Markit1: TAction;
    SaveMark1: TAction;
    SaveMark2: TAction;
    SaveMark3: TAction;
    SaveMark4: TAction;
    SaveMark5: TAction;
    NextPage1: TAction;
    PrePage1: TAction;
    nextline1: TAction;
    preline1: TAction;
    AutoCrossPage1: TAction;
    VoiceSpeak1: TAction;
    VoiceStop1: TAction;
    ExitFullScreen: TAction;
    VoiceSpeedAdd: TAction;
    VoiceSpeedMul: TAction;
    VoiceVolAdd: TAction;
    VoiceVolMul: TAction;
    NextParagraph: TAction;
    FindBar: TbsSkinPanel;
    bsSkinButton1: TbsSkinButton;
    findpos: TbsSkinLabel;
    findtext: TbsSkinEdit;
    findnext: TbsSkinButton;
    findpre: TbsSkinButton;
    athead: TbsSkinCheckRadioBox;
    RegExp1: TRegExp;
    bsSkinMenuSpeedButton2: TbsSkinMenuSpeedButton;
    HelpList: TbsSkinPopupMenu;
    H6: TMenuItem;
    W1: TMenuItem;
    N16: TMenuItem;
    A1: TMenuItem;
    HelpCtrl: TAction;
    CheckWeb: TAction;
    About: TAction;
    IdHTTP1: TIdHTTP;
    G1: TMenuItem;
    G2: TMenuItem;
    B1: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    F1: TMenuItem;
    FileManager1: TAction;
    RebuildDatabase: TAction;
    ArchDatabase: TAction;
    Saveas: TAction;
    O2: TMenuItem;
    rb1: TMenuItem;
    ad1: TMenuItem;
    sa1: TMenuItem;
    SaveDialog1: TbsSkinSaveDialog;
    bsSkinSpeedButton4: TbsSkinSpeedButton;
    procedure ReadMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure bsSkinSpeedButton2Click(Sender: TObject);
    procedure bsSkinSpeedButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure process1Change(Sender: TObject);
    procedure ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
    procedure process2Click(Sender: TObject);
    procedure bsSkinEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure FormClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure R11Click(Sender: TObject);
    procedure ClearStatusTimer(Sender: TObject);
    procedure RunTimeTimer(Sender: TObject);
    procedure Memory1Timer(Sender: TObject);
    procedure AutoMark1Timer(Sender: TObject);
    procedure ReadChanged(Sender: TObject);
    procedure ReadClick(Sender: TObject);
    procedure bsBusinessSkinForm1Maximize(Sender: TObject);
    procedure bsBusinessSkinForm1Restore(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure N3Click(Sender: TObject);
    procedure H1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure bsSkinSpeedButton1Click(Sender: TObject);
    procedure MainPopPopup(Sender: TObject);
    procedure Query1DeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ShowHideClick(Sender: TObject);
    procedure TraypopPopup(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure bsBusinessSkinForm1Minimize(Sender: TObject);
    procedure MainScrollbarChange(Sender: TObject);
    procedure MainScrollbarPageDown(Sender: TObject);
    procedure MainScrollbarPageUp(Sender: TObject);
    procedure MainScrollbarUpButtonClick(Sender: TObject);
    procedure MainScrollbarDownButtonClick(Sender: TObject);
    procedure MenuButtonClick(Sender: TObject);
    procedure process1DownButtonClick(Sender: TObject);
    procedure process1UpButtonClick(Sender: TObject);
    procedure process1PageUp(Sender: TObject);
    procedure process1PageDown(Sender: TObject);
    procedure OpenFile1Execute(Sender: TObject);
    procedure FullScreen1Execute(Sender: TObject);
    procedure Find1Execute(Sender: TObject);
    procedure Copy1Execute(Sender: TObject);
    procedure Markit1Execute(Sender: TObject);
    procedure SaveMark1Execute(Sender: TObject);
    procedure NextPage1Execute(Sender: TObject);
    procedure PrePage1Execute(Sender: TObject);
    procedure nextline1Execute(Sender: TObject);
    procedure preline1Execute(Sender: TObject);
    procedure AutoCrossPage1Execute(Sender: TObject);
    procedure VoiceSpeak1Execute(Sender: TObject);
    procedure VoiceStop1Execute(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure ExitFullScreenExecute(Sender: TObject);
    procedure VoiceVolAddExecute(Sender: TObject);
    procedure VoiceVolMulExecute(Sender: TObject);
    procedure VoiceSpeedAddExecute(Sender: TObject);
    procedure VoiceSpeedMulExecute(Sender: TObject);
    procedure NextParagraphExecute(Sender: TObject);
    procedure findnextClick(Sender: TObject);
    procedure findpreClick(Sender: TObject);
    procedure findtextChange(Sender: TObject);
    procedure findtextKeyPress(Sender: TObject; var Key: Char);
    procedure bsSkinButton1Click(Sender: TObject);
    procedure AboutExecute(Sender: TObject);
    procedure HelpCtrlExecute(Sender: TObject);
    procedure CheckWebExecute(Sender: TObject);
    procedure G2Click(Sender: TObject);
    procedure B1Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure FileManager1Execute(Sender: TObject);
    procedure SaveasExecute(Sender: TObject);
    procedure RebuildDatabaseExecute(Sender: TObject);
    procedure ArchDatabaseExecute(Sender: TObject);
    procedure ReadMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ReadMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ReadReadComplete(Sender: TObject);
  private
    { Private declarations }
    MinRunTime:Boolean;
    MinCross:Boolean;
    CrossPause:Boolean;
    procedure Mouse_Scroll(var Msg: tagMSG; var Handled: Boolean);
    function CloseFile:boolean;
    function OpenMark(num:integer):boolean;
    function SaveMark(num:integer):boolean;
    function checkCodes(FS:TFilestream):integer;
    procedure LoadHistory;
    procedure RepairDataBase;
    procedure WMDROPFILES(var Message:TWMDROPFILES);message WM_DROPFILES;
    procedure WMMOVE(var Msg:TMessage);message WM_MOVE;
    procedure Find(next:boolean);
    procedure updateDatabase();
  public
    { Public declarations }
    HeadExt:TStringList;
    TailExt:TStringList;            
    procedure StringtoFont(S:string;var Font:TFont);
    procedure FonttoString(Font:TFont;var S:string);
    function OpenFile(Filename:string):boolean;
    procedure DeleteMark(num:integer);
    procedure DeleteAllMarks;
    procedure LoadMarkList(LoadType:integer);
    procedure UpdateMarkList;
    procedure WMHotKey(var Msg: TWMHotKey); message WM_HOTKEY;
  end;

TReadInfo = function(Ver:integer) : PChar;
TReadFile = function (Filename:Pchar): Pchar;

procedure ShortCutToKey(ShortCut: TShortCut; var Key: Word; var Shift: TShiftState);
function ShiftStateToWord(TShift: TShiftState): Word;

var
  Reader: TReader;
  BossKey: Integer;
  Key, Shift: Word;
  T:TShiftState;

implementation
  uses ShellAPI,publicvar,setform,About,MarkForm,CopyForm,MenuForm,VoiceSetForm,
  FileManagerForm;

{$R *.dfm}

procedure ShortCutToKey(ShortCut: TShortCut; var Key: Word; var Shift: TShiftState);
begin
  Key := ShortCut and not (scShift + scCtrl + scAlt);
  Shift := [];
  if ShortCut and scShift <> 0 then Include(Shift, ssShift);
  if ShortCut and scCtrl <> 0 then Include(Shift, ssCtrl);
  if ShortCut and scAlt <> 0 then Include(Shift, ssAlt);
end;

function ShiftStateToWord(TShift: TShiftState): Word;
begin
  Result := 0;
  if ssShift in TShift then Result := MOD_SHIFT;
  if ssCtrl in TShift then Result := Result or MOD_CONTROL;
  if ssAlt in TShift then Result:= Result or MOD_ALT;
end;

procedure TReader.VoiceStop1Execute(Sender: TObject);
begin
  Read.Stop;
end;

procedure TReader.VoiceVolAddExecute(Sender: TObject);
begin
  if SpVoice1.Volume<100 then
  begin
    SystemSets.VoiceVol:=SystemSets.VoiceVol+1;
    SpVoice1.Volume:=SpVoice1.Volume+1;
  end;
end;

procedure TReader.VoiceVolMulExecute(Sender: TObject);
begin
  if SpVoice1.Volume>0 then
  begin
    SystemSets.VoiceVol:=SystemSets.VoiceVol-1;
    SpVoice1.Volume:=SpVoice1.Volume-1;
  end;
end;

procedure TReader.AboutExecute(Sender: TObject);
begin
  AboutBox.ShowModal;
end;

procedure TReader.ApplicationEvents1Message(var Msg: tagMSG;
  var Handled: Boolean);
begin
  if (Reader.Active)and(not FindText.Focused) then Mouse_Scroll(msg,handled);
  inherited;
end;

procedure TReader.ArchDatabaseExecute(Sender: TObject);
begin
  Query1.SQL.Text:='VACUUM';
  Query1.ExecSQL;
  RMessage.MessageDlg('压缩数据库完成！',mtInformation,[mbOK],0);
end;

procedure TReader.AutoCrossPage1Execute(Sender: TObject);
begin
  if AutoCrossPage1.Tag=0 then
  begin
    AutoCrossPage1.Tag:=1;
    read.AutoCross:=True;
  end else begin
    AutoCrossPage1.Tag:=0;
    read.AutoCross:=False;
  end;
end;

procedure TReader.AutoMark1Timer(Sender: TObject);
begin
  if Read.Active then SaveMark(0);
end;

procedure TReader.B1Click(Sender: TObject);
begin
  Read.GB:=2;
  Reader.Repaint;
end;

procedure TReader.bsBusinessSkinForm1Maximize(Sender: TObject);
begin
  SetWindowPos(handle,HWND_TOPMOST,Left,Top,Width,Height,0);
  BorderStyle:=bsNone;
  bsSkinStatusBar1.Visible:=false;
end;

procedure TReader.bsBusinessSkinForm1Minimize(Sender: TObject);
begin
  if Read.AutoCross then
  begin
    Read.AutoCross:=false;
    MinCross:=True;
  end;
  if RunTime.Enabled then
  begin
    RunTime.Enabled:=False;
    MinRunTime:=True;    
  end;
end;

procedure TReader.bsBusinessSkinForm1Restore(Sender: TObject);
begin
  SetWindowPos(handle,HWND_NOTOPMOST,Left,Top,Width,Height,0);
  BorderStyle:=bsSizeable;
  bsSkinStatusBar1.Visible:=true;
  if MinCross then
  begin
    MinCross:=False;
    Read.AutoCross:=True;
  end;
  if MinRunTime then
  begin
    MinRunTime:=False;
    RunTime.Enabled:=True;
  end;
end;

procedure TReader.bsSkinButton1Click(Sender: TObject);
begin
  FindBar.Hide;
end;

procedure TReader.bsSkinEdit1KeyPress(Sender: TObject; var Key: Char);
var
  i:int64;
begin
  if key=#27 then bsskinedit1.Visible:=false;
  if ((key<'0') or (key>'9')) and (key<>#13) and (key<>#8) then
    begin
      key:=#0;
    end else
  if key=#13 then
  begin
    try
      i:=strtoint(bsskinedit1.Text);
    except
      i:=0;
    end;
    if i<process1.Max then
      begin
        Read.Pos:=i;
        bsskinedit1.Visible:=false;
      end
      else bsskinedit1.SelectAll;
  end;

end;

procedure TReader.bsSkinSpeedButton1Click(Sender: TObject);
begin
  if MarkF.Visible=False then
    begin
      MarkF.left :=Reader.Left - MarkF.width;
      if MarkF.Left + MarkF.Width < 50 then
        MarkF.Left := 0;
      MarkF.top:=Reader.Top;
      MarkF.height:=Reader.Height;
      LoadMarkList(1);
    end;
  MarkF.Visible:=not MarkF.Visible;
  if (MarkF.Visible) and (bsBusinessSkinForm1.WindowState=wsMaximized) then
  begin
    MarkF.Left:=0;
  end;
end;

procedure TReader.bsSkinSpeedButton2Click(Sender: TObject);
var
  settingf:Tsetting;
begin
  Toolbar.Visible:=false;
  settingf:=Tsetting.Create(nil);
  settingf.ShowModal;
  settingf.Free;
end;

procedure TReader.bsSkinSpeedButton3Click(Sender: TObject);
begin
  close;
end;

procedure TReader.MenuButtonClick(Sender: TObject);
var
  MygetMenu:TgetMenu;
begin
  ToolBar.Visible:=False;
  MygetMenu:=TgetMenu.Create(self);
  MygetMenu.ShowModal;
  MygetMenu.Free;
end;

function TReader.checkCodes(FS: TFilestream): integer;
var
  u8,un:AnsiString;
begin
  FS.Seek(0,0);
  SetLength(u8,3);
  FS.Read(u8[1],3);
  SetLength(un,2);
  FS.Seek(0,0);
  FS.Read(un[1],2);
  if un=#$FF#$FE then result:=Unicode else
  if u8=#$EF#$BB#$BF then result:=utf8 else
  result:=ansi;
end;

procedure TReader.CheckWebExecute(Sender: TObject);
var
  TS:TStringList;
  id:integer;
begin
  TS:=TStringList.Create;
  try
    IdHttp1.ReadTimeout:=1000;
    TS.Text:=IdHttp1.Get('http://soft.awaysoft.com/reader/version.txt');
    id:=strtoint(TS.Values['id']);
  except
    id:=VersionID;
  end;
  if id>VersionID then begin
     if RMessage.MessageDlg('发现新版本'+TS.Values['version']+'，是否转到下载页？',mtConfirmation,[mbYES,mbNO],0)=mrYES then
        shellexecute(handle,pchar('open'),pchar('http://www.awaysoft.com/download.php'),nil,nil,9);
  end else RMessage.MessageDlg('您使用的已经是最新版了，无需更新',mtInformation,[mbOK],0);
  TS.Free;
end;

procedure TReader.ClearStatusTimer(Sender: TObject);
var
  i:integer;
begin
  ClearStatus.Tag:=ClearStatus.Tag+1;
  if ClearStatus.Tag>4 then
    ClearStatus.Tag:=0;
  i:=RunTime.Tag;
  case ClearStatus.Tag of
    0:status.Caption:='欢迎使用'+Caption+'！';
    1:status.Caption:='涛儿软件工作室出品';
    2:status.Caption:='现在时间：'+Timetostr(now);
    3:status.Caption:='目前已经阅读时间：'+inttostr(i div 3600)+'小时'+inttostr(i mod 3600 div 60)+'分'+inttostr(i mod 60)+'秒';
    4:status.Caption:='当前文件：'+OpenDialog1.Filename;
  end;
  Status.Hint:=Status.Caption;
end;

function TReader.CloseFile: boolean;
var
  fonts:string;
  Filename:string;
begin
  if LastFileName<>'' then
    Filename:=LastFileName
  else Filename:=OpenDialog1.FileName;
  if FileName='' then
  begin
    result:=false;
    exit;
  end;
  try
    SaveMark(0);
    Query1.Close;
    Query1.SQL.Clear;
    Fonttostring(Systemsets.Font,fonts);
    Query1.SQL.Add('UPDATE fileinfo SET 背景颜色="'+inttostr(SystemSets.Color)+'",背景类型="'+booleantostr(SystemSets.BgType)+'",背景模式="'+inttostr(SystemSets.BgMode)+'",背景图片="'+SystemSets.BgFile+'",字体="'+fonts+'",大小="'+inttostr(Read.Length)+'" WHERE id="'+inttostr(SystemSets.FileId)+'"');
    Query1.Open;
    if FileName<>H1.Caption then
    begin
      Query1.Close;
      Query1.SQL.Clear;
      Query1.SQL.Add('delete from historyinfo where 文件名="'+Filename+'";');
      Query1.Open;
      Query1.Close;
      Query1.SQL.Clear;
      Query1.SQL.Add('insert into historyinfo(文件名) values("'+Filename+'");');
      Query1.Open;
    end;
  except

  end;
  Read.Active:=false;
  Read.Text:='';
  Read.Pos:=1;
  Read.AutoCross:=False;
  LastFileName:='';
  SystemSets.FileID:=-1;
  N3.Enabled:=false;
  process1.Max:=2;
  process1.Position:=1;
  MainScrollBar.Max:=2;
  MainScrollBar.Position:=1;
  process2.Caption:='1';
  MenuButton.Enabled:=False;
  SaveMark1.Enabled:=False;
  SaveMark2.Enabled:=False;
  SaveMark3.Enabled:=False;
  SaveMark4.Enabled:=False;
  SaveMark5.Enabled:=False;
  LoadHistory;
  O1.Click;
  result:=true;
end;

procedure TReader.Copy1Execute(Sender: TObject);
var
  CPF:TCopyText;
begin
  CPF:=TCopyText.Create(self);
  CPF.Memo.Clear;
  CPF.Memo.Lines.Add(copy(Read.Text,Read.Pos,5000));
  CPF.Memo.SelectAll;
  CPF.ShowModal;
  CPF.Free;
end;

procedure TReader.DeleteAllMarks;
begin
  if OpenDialog1.FileName='' then exit;
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('Delete from markinfo where id="'+inttostr(SystemSets.FileID)+'" and number="6"');
  try
    Query1.Open;
  except
    RMessage.MessageDlg('删除标签出错！',mtError,[mbOK],0);
  end;
  Query1.SQL.Text:='VACUUM';
  Query1.ExecSQL;
  LoadMarkList(1);
end;

procedure TReader.DeleteMark(num: integer);
begin
  if OpenDialog1.FileName='' then exit;
  if num>BookMarks.Count-1 then
    exit;
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('Delete from markinfo where id="'+inttostr(SystemSets.FileID)+'" and position="'+BookMarks[num]+'"');
  try
    Query1.Open;
  except
    RMessage.MessageDlg('删除标签出错！',mtError,[mbOK],0);
  end;
  LoadMarkList(1);
end;

procedure TReader.ExitFullScreenExecute(Sender: TObject);
begin
  if bsbusinessSkinForm1.WindowState=wsMaximized then
    FullScreen1.OnExecute(FullScreen1);
end;

procedure TReader.process1Change(Sender: TObject);
begin
  process1.Hint:='当前进度:'+inttostr(process1.Position*100 div process1.Max)+'%';
  process2.Caption:=inttostr(process1.Position)+'/'+inttostr(process1.Max)+' '+format('%.2f',[process1.Position/process1.Max*100])+'%';
  if Read.Pos<>process1.Position then begin 
    if process1.Position<>process1.Max then Read.Pos:=process1.Position
    else Read.Pos:=process1.Position-1;
  end;
end;

procedure TReader.process1DownButtonClick(Sender: TObject);
begin
  Read.preLine(1);
end;

procedure TReader.process1PageDown(Sender: TObject);
begin
  Read.nextPage;
end;

procedure TReader.process1PageUp(Sender: TObject);
begin
  Read.prePage;
end;

procedure TReader.process1UpButtonClick(Sender: TObject);
begin
  Read.nextLine(1);
end;

procedure TReader.process2Click(Sender: TObject);
begin
  bsskinedit1.Text:=inttostr(process1.Position);
  bsskinedit1.Top:=process2.Top;
  bsskinedit1.Left:=process2.Left;
  bsskinedit1.Height:=process2.Height;
  bsskinedit1.Width:=process2.Width;
  bsskinedit1.Visible:=true;
  bsskinedit1.SetFocus;
  bsskinedit1.SelectAll;
end;

procedure TReader.Query1DeleteError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  RMessage.MessageDlg(E.Message,mtError,[mbOK],0);
end;

procedure TReader.FileManager1Execute(Sender: TObject);
begin
  FileManager.Visible:=not FileManager.Visible;
end;

procedure TReader.Find(next: boolean);
var
  s1,s2:WideString;
begin
  if FindObj.Count=0 then
  begin
    s1:=Read.Text;
    s2:=FindText.Text;
    RegExp1.Global := true;
    RegExp1.Pattern:=s2;
    RegExp1.IgnoreCase := true;
    FindObj.machs := RegExp1.Execute(s1) as IMatchCollection;
    status.Caption:='共搜索到'+inttostr(FindObj.machs.count)+'处';
    FindObj.Count:=FindObj.machs.Count;
    FindObj.Findpos:=-1;
  end;
  FindObj.Findpos:=-1;
  if (FindObj.Count>0) then
  while (FindObj.Findpos<FindObj.Count-1)and((FindObj.machs.Item[FindObj.Findpos+1] as Match).FirstIndex<Read.Pos) do
    inc(FindObj.Findpos);
  if athead.Checked then
  begin
    athead.Checked:=false;
    FindObj.Findpos:=-1;
  end;
  if FindObj.Count=0 then RMessage.MessageDlg('没有找到搜索的内容！',mtInformation,[mbOK],0)
  else begin
    if next then
      if FindObj.findpos>=FindObj.Count-1 then
        status.Caption:='已经搜索到文章最后'
      else begin
        inc(FindObj.findpos);
        Read.Pos:=(FindObj.machs.Item[FindObj.findpos] as Match).FirstIndex+1;
        status.Caption:=(FindObj.machs.Item[FindObj.findpos] as Match).Value;
      end
    else if FindObj.findpos<=0 then
        status.Caption:='已经搜索到文章开头'
      else begin
        FindObj.findpos:=FindObj.findpos-1;
        Read.Pos:=(FindObj.machs.Item[FindObj.findpos] as Match).FirstIndex+1;
      end;
  end;
end;

procedure TReader.Find1Execute(Sender: TObject);
begin
  FindBar.Visible:=not FindBar.Visible;
  if FindBar.Visible then
    FindText.SetFocus;
end;

procedure TReader.findnextClick(Sender: TObject);
begin
  Find(true);
end;

procedure TReader.findpreClick(Sender: TObject);
begin
  Find(false);
end;

procedure TReader.findtextChange(Sender: TObject);
begin
  FindObj.Findpos:=-1;
  FindObj.Count:=0;
end;

procedure TReader.findtextKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
    Find(true);
end;

procedure TReader.FonttoString(Font: TFont; var S: string);
var
  i:integer;
begin
  s:=Font.Name+'|'+inttostr(font.Size)+'|';
  i:=0;
  if fsBold in Font.Style then i:=1;
  if fsItalic in Font.Style then i:=i+(1 shl 1);
  if fsUnderline in Font.Style then i:=i+(1 shl 2);
  if fsStrikeOut in Font.Style then i:=i+(1 shl 3);
  s:=s+inttostr(i)+'|'+inttostr(font.Color);
end;

procedure TReader.FormClick(Sender: TObject);
begin
  bsskinedit1.Visible:=false;
end;

procedure TReader.FormClose(Sender: TObject; var Action: TCloseAction);
var
  sql:string;
  fonts:string;
begin
  CloseFile;
  Query1.Close;
  Query1.SQL.Clear;
  with SystemSets do
  begin
  try
  FonttoString(Font,fonts);
  sql:='update systeminfo set 标题="'+caption+'",字体="'+fonts+'",背景颜色="'+inttostr(Color)+'",背景模式="'+inttostr(BgMode)+'",行距="'+inttostr(RowSpacing)+'",列距="'+inttostr(ListSpacing)+'",背景类型="'+booleantostr(BgType)+'",背景图片="'+BgFile+'",' +
       '自动清理内存="'+booleantostr(Memory)+'",自动全屏="'+booleantostr(AutoFullScreen)+'",自动保存书签="'+booleantostr(AutoSaveMark)+'",自动翻页="'+booleantostr(AutoCrossPage)+'",翻页类型="'+inttostr(CrossType)+'",翻页时间="'+inttostr(CrossTime)+'",' +
       'x="'+inttostr(left)+'",y="'+inttostr(top)+'",height="'+inttostr(height)+'",width="'+inttostr(width)+'",最后打开="'+OpenDialog1.FileName+' '+'",皮肤="'+SystemSets.SkinFile+'", 语音引擎="'+inttostr(SystemSets.VoiceIndex)+
       '", 音速="'+inttostr(SystemSets.VoiceRate)+'", 音量="'+inttostr(SystemSets.VoiceVol)+'", 滚动条="'+booleantostr(SystemSets.ScrollBar)+'", 定时提醒="'+inttostr(SystemSets.AwokeHour)+'", ' +
       '统一字体="'+booleantostr(SystemSets.UseOneFont)+'", 一个实例="'+booleantostr(OnyOne)+'" WHERE rowid=1';
  except
  end;
  end;
  try
    query1.SQL.Add(sql);
    query1.Open;
    Query1.SQL.Text:='update systeminfo2 set value='''+SystemSets.DetectDir+''' where setname=''DetectDir''';
    Query1.ExecSQL;
    Query1.SQL.Text:='update systeminfo2 set value='''+inttostr(SystemSets.MarginLeft)+''' where setname=''MarginLeft''';
    Query1.ExecSQL;
    Query1.SQL.Text:='update systeminfo2 set value='''+inttostr(SystemSets.MarginRight)+''' where setname=''MarginRight''';
    Query1.ExecSQL;
    Query1.SQL.Text:='update systeminfo2 set value='''+inttostr(SystemSets.MarginTop)+''' where setname=''MarginTop''';
    Query1.ExecSQL;
    Query1.SQL.Text:='update systeminfo2 set value='''+inttostr(SystemSets.MarginBottom)+''' where setname=''MarginBottom''';
    Query1.ExecSQL;
  except
    RMessage.MessageDlg('保存数据出错，请尝试修复数据库！',mtError,[mbOK],0);
  end;
end;

procedure TReader.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  bsbusinessskinform1.WindowState:=wsNormal;
end;

procedure TReader.FormCreate(Sender: TObject);
const
  helpstring='参数说明：'+#13+'-h:显示本信息'+#13+'-r:重建数据库'+#13+'<filename>:打开此文件';
var
  fonts:string;
  Database,DriverDLL:string;
  LibHandle:THandle;
  PluginType,UrlPosition,UrlKeyWord:TReadInfo;
  i,n:integer;
  DllFile,FilterString:string;
  CaptionTemp:String;
  FileHandle:integer;
  FileSize:DWORD;
  setname,value:string;
  AppDataPath: String;
begin
  MinRunTime:=False;
  MinCross:=False;
  CrossPause:=False;
  AppDataPath := GetUserPath (CSIDL_LOCAL_APPDATA) + '\AwayReader\';
  if not DirectoryExists(AppDataPath) then
    CreateDirectory(PWideChar(AppDataPath), nil);
  Database:=AppDataPath+'data.dat';
  DriverDLL:=ExtractFilePath(ParamStr(0))+'sqlite3.dll';
  if not FileExists(DriverDLL) then
  begin
    RMessage.MessageDlg('数据库驱动文件丢失，请重新安装本程序，或者从SQLite官网上下载sqllite3.dll，并复制到本程序目录！',mtError,[mbOK],0);
    ExitProcess(0);
  end;
  if (ParamCount>0)and(ParamStr(1)='-h') then
  begin
    RMessage.MessageDlg(helpstring,mtInformation,[mbOK],0);
    ExitProcess(0);
  end;
  if (ParamCount>0)and(ParamStr(1)='-r') then
  begin
    DeleteFile(Database);
    RepairDataBase;
    RMessage.MessageDlg('重建数据库成功！',mtInformation,[mbOK],0);
    ExitProcess(0);
  end;
  FileHandle:=FileOpen(Database,0);
  FileSize:=GetFileSize(FileHandle,nil);
  FileClose(FileHandle);
  if FileSize=0 then
  begin
    DeleteFile(Database);
    RepairDataBase;
  end;
  DB1.Database:=Database;
  DB1.DriverDLL:=DriverDLL;
  if not FileExists(Database) then
  begin
    DB1.Connected:=true;
    RepairDataBase;
  end else DB1.Connected:=true;
  updateDatabase;//判断3.0之后的表是否存在
  with SystemSets do
    begin
      FileId:=-1;
      Font:=TFont.Create;
      Query1.Close;
      Query1.SQL.Clear;
      Query1.SQL.Add('select * from systeminfo');
      Query1.Open;
      CaptionTemp:=Query1.FieldByName('标题').Value;
      fonts:=Query1.FieldByName('字体').Value;
      Color:=Query1.FieldByName('背景颜色').Value;
      RowSpacing:=Query1.FieldByName('行距').Value;
      ListSpacing:=Query1.FieldByName('列距').Value;
      BgType:=Query1.FieldByName('背景类型').Value;
      try
        BgFile:=Query1.FieldByName('背景图片').Value;
        if FileExists(BgFile) then
          Read.BgFile:=BgFile;
      except
        BgFile:='';
      end;
      BgMode:=Query1.FieldByName('背景模式').Value;
      Memory:=Query1.FieldByName('自动清理内存').Value;
      Memory1.Enabled:=Memory;
      AutoFullScreen:=Query1.FieldByName('自动全屏').Value;
      AutoSaveMark:=Query1.FieldByName('自动保存书签').Value;
      AutoMark1.Enabled:=AutoSaveMark;
      AutoCrossPage:=Query1.FieldByName('自动翻页').Value;
      CrossType:=Query1.FieldByName('翻页类型').Value;
      CrossTime:=Query1.FieldByName('翻页时间').Value;
      VoiceIndex:=Query1.FieldByName('语音引擎').Value;
      VoiceRate:=Query1.FieldByName('音速').Value;
      VoiceVol:=Query1.FieldByName('音量').Value;
      LastCloseFile:=Query1.FieldByName('最后打开').Value;
      SkinFile:=Query1.FieldByName('皮肤').Value;
      ScrollBar:=Query1.FieldByName('滚动条').Value;
      AwokeHour:=Query1.FieldByName('定时提醒').Value;
      UseOneFont:=Query1.FieldByName('统一字体').Value;
      OnyOne:=Query1.FieldByName('一个实例').Value;
      StringtoFont(fonts,Font);
      Read.Active:=false;
      Read.Font:=Font;
      Read.Color:=Color;
      Read.BgMode:=BgMode;
      Read.BgType:=BgType;
      Read.Rows:=RowSpacing;
      Read.Cols:=ListSpacing;
      Read.CrossTime:=CrossTime;
      Read.CrossType:=CrossType;
      Read.AutoCross:=AutoCrossPage;
    end;
  Caption:=CaptionTemp;
  Left:=Query1.FieldByName('x').Value;
  Top:=Query1.FieldByName('y').Value;
  Height:=Query1.FieldByName('height').Value;
  Width:=Query1.FieldByName('width').Value;
  MainScrollBar.Left:=ClientWidth-MainScrollBar.Width;
  MainScrollBar.Top:=0;
  MainScrollBar.Height:=Read.Height;
  Read.VoiceRate:=SystemSets.VoiceRate;
  Read.VoiceVol:=SystemSets.VoiceVol;
  Read.VoiceIndex:=SystemSets.VoiceIndex;
  BookMarks:=TStringList.Create;
  Application.HintPause:=0;
  LoadHistory;
  DragAcceptFiles(Handle, True);
  if SystemSets.SkinFile<>'0' then
  begin
    bsSkinData1.LoadFromCompressedFile(SystemSets.SkinFile);
    N5.Visible:=True;
  end;
  //载入配置2
  //赋初始值
  SystemSets.DetectDir:='|';
  SystemSets.MarginLeft:=-1;
  SystemSets.MarginRight:=-1;
  SystemSets.MarginTop:=-1;
  SystemSets.MarginBottom:=-1;

  //查询
  Query1.SQL.Text:='select * from systeminfo2';
  Query1.Open;
  while not Query1.Eof do begin
    setname:=Query1.FieldByName('setname').Value;
    value:=Query1.FieldByName('value').Value;
    if setname='DetectDir' then
      SystemSets.DetectDir:=value;
    if setname='MarginLeft' then
      SystemSets.MarginLeft:=strtoint(value);
    if setname='MarginRight' then
      SystemSets.MarginRight:=strtoint(value);
    if setname='MarginTop' then
      SystemSets.MarginTop:=strtoint(value);
    if setname='MarginBottom' then
      SystemSets.MarginBottom:=strtoint(value);
    Query1.Next;
  end;
  //对没有找到的条目更新
  with SystemSets do begin
    if DetectDir='|' then
      try
        DetectDir:='';
        Query1.SQL.Text:='insert into systeminfo2(setname,value) values(''DetectDir'','''');';
        Query1.ExecSQL;
      except
      end;
    if MarginLeft=-1 then
      try
        MarginLeft:=3;
        Query1.SQL.Text:='insert into systeminfo2(setname,value) values(''MarginLeft'',''0'');';
        Query1.ExecSQL;
      except
      end;
    if MarginRight=-1 then
      try
        MarginRight:=3;
        Query1.SQL.Text:='insert into systeminfo2(setname,value) values(''MarginRight'',''0'');';
        Query1.ExecSQL;
      except
      end;
    if MarginTop=-1 then
      try
        MarginTop:=3;
        Query1.SQL.Text:='insert into systeminfo2(setname,value) values(''MarginTop'',''0'');';
        Query1.ExecSQL;
      except
      end;
    if MarginBottom=-1 then
      try
        MarginBottom:=3;
        Query1.SQL.Text:='insert into systeminfo2(setname,value) values(''MarginBottom'',''0'');';
        Query1.ExecSQL;
      except
      end;
  end;

  Read.Margins.Left:=SystemSets.MarginLeft;
  Read.Margins.Right:=SystemSets.MarginRight;
  Read.Margins.Top:=SystemSets.MarginTop;
  Read.Margins.Bottom:=SystemSets.MarginBottom;

  //载入快捷键
  for i := 1 to 18 do
  begin
    Query1.Close;
    Query1.SQL.Text:='select * from shortcutinfo where id='+inttostr(i);
    Query1.Open;
    if not Query1.IsEmpty then
      MyShortCut[i]:=Query1.FieldByName('ShortCut').Value
    else MyShortCut[i]:=ShortCutDome[i];
    case i of
      1:begin
        bosskey := GlobalAddAtom('MyHotKey') - $C000;
        ShortCutToKey(MyShortCut[i], Key, T);
        Shift := ShiftStateToWord(T);
        RegisterHotKey(Handle, bosskey, Shift, Key);
      end;
      2:OpenFile1.ShortCut:=MyShortCut[i];
      3:FullScreen1.ShortCut:=MyShortCut[i];
      4:Find1.ShortCut:=MyShortCut[i];
      5:Copy1.ShortCut:=MyShortCut[i];
      6:Markit1.ShortCut:=MyShortCut[i];
      7:NextPage1.ShortCut:=MyShortCut[i];
      8:PrePage1.ShortCut:=MyShortCut[i];
      9:nextline1.ShortCut:=MyShortCut[i];
      10:preline1.ShortCut:=MyShortCut[i];
      11:AutoCrossPage1.ShortCut:=MyShortCut[i];
      12:VoiceSpeak1.ShortCut:=MyShortCut[i];
      13:VoiceStop1.ShortCut:=MyShortCut[i];
      14:VoiceSpeedAdd.ShortCut:=MyShortCut[i];
      15:VoiceSpeedMul.ShortCut:=MyShortCut[i];
      16:VoiceVolAdd.ShortCut:=MyShortCut[i];
      17:VoiceVolMul.ShortCut:=MyShortCut[i];
      18:NextParagraph.ShortCut:=MyShortCut[i];
    end;
  end;
  //开始载入插件
  HeadExt:=TStringList.Create;
  TailExt:=TStringList.Create;
  FilterString:='所有支持的格式|*.txt';
  with TiniFile.Create(ExtractFilePath(ParamStr(0))+'plugins.ini') do
  begin
    n:=ReadInteger('插件','数目',0);
    if n<>0 then
    for i := 1 to n do
    begin
      DllFile:=ExtractFilePath(ParamStr(0))+ReadString('插件',inttostr(i),'');
      if FileExists(DllFile) then
      begin
        LibHandle:=LoadLibrary(pchar(DllFile));
        try
          if LibHandle<>0 then
          begin
            @PluginType:=GetProcAddress(LibHandle,'PluginType');
            if not (@PluginType = nil) then
            begin
            if PluginType(3)='FileOpen' then
              begin
                @UrlPosition:=GetProcAddress(LibHandle,'UrlPosition');
                if (not (@UrlPosition=nil)) and (UrlPosition(3)='head') then
                begin
                  @UrlKeyWord:=GetProcAddress(LibHandle,'UrlKeyWord');
                  if not (@UrlKeyWord=nil) then HeadExt.Add(UrlKeyWord(3)+'='+DllFile);
                end else if not (@UrlPosition=nil) then begin
                  @UrlKeyWord:=GetProcAddress(LibHandle,'UrlKeyWord');
                  TailExt.Add(UrlKeyWord(3)+'='+DllFile);
                  OpenDialog1.Filter:=copy(UrlKeyWord(3),2,length(UrlKeyWord(3))-1)+'文件(*'+UrlKeyWord(3)+')|*'+UrlKeyWord(3)+'|'+OpenDialog1.Filter;
                  FilterString:=FilterString+';*'+UrlKeyWord(3);
                end;
              end;
            end;
          end;
        finally
          FreeLibrary(LibHandle);
        end;
      end;
    end;
  end;
  OpenDialog1.Filter:=FilterString+'|'+OpenDialog1.Filter;
  status.Caption:='程序启动完成！';
  if (ParamCount>0)and(ParamStr(1)[1]<>'-') then
    begin
      OpenDialog1.FileName:=trim(ParamStr(1));
      if not OpenFile(OpenDialog1.FileName) then OpenDialog1.FileName:='';
    end else begin
      if Trim(SystemSets.LastCloseFile)<>'' then
        if RMessage.MessageDlg('是否继续上次阅读？',mtConfirmation,[mbYES,mbNO],0)=mrYES then
           begin
             OpenDialog1.FileName:=trim(SystemSets.LastCloseFile);
             if not OpenFile(OpenDialog1.FileName) then OpenDialog1.FileName:='';
           end;
    end;
  bsTrayIcon1.IconVisible:=True;
  Reader.DoubleBuffered:=true;
  Read.Tag:=GetTickCount;
  Application.HinthidePause:=10000;
end;

procedure TReader.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if y<5 then
    ToolBar.Visible:=true
  else ToolBar.Visible:=false;
end;

procedure TReader.FullScreen1Execute(Sender: TObject);
begin
  if bsBusinessSkinForm1.WindowState=wsMaximized then
    bsBusinessSkinForm1.WindowState:=wsNormal
  else bsBusinessSkinForm1.WindowState:=wsMaximized;
end;

procedure TReader.G2Click(Sender: TObject);
begin
  Read.GB:=1;
  Reader.Repaint;
end;

procedure TReader.H1Click(Sender: TObject);
begin
  OpenDialog1.FileName:=(Sender as TMenuItem).Caption;
  OpenFile(OpenDialog1.FileName);
end;

procedure TReader.HelpCtrlExecute(Sender: TObject);
begin
  shellexecute(handle,pchar('open'),pchar('http://www.taorsoft.cn/help.php'),nil,nil,9);
end;

procedure TReader.LoadHistory;
var
  i,j:integer;
  s:array[1..5] of string;
begin
  for i := 1 to 5 do
    s[i]:='';
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('select * from historyinfo order by id desc');
  Query1.Open;
  i:=1;j:=1;
  while (not Query1.Eof)and (j<=5) do
  begin
    i:=Query1.FieldByName('id').Value;
    s[j]:=Query1.FieldByName('文件名').Value;
    j:=j+1;
    Query1.Next;
  end;
  for j := 0 to HistoryList.Items.Count-1 do
    if HistoryList.Items[j].Tag>0 then begin
      HistoryList.Items[j].Caption:=s[HistoryList.Items[j].Tag];
      if HistoryList.Items[j].Caption='' then begin
        HistoryList.Items[j].Caption:='--无记录--';
        HistoryList.Items[j].Enabled:=false;
        if HistoryList.Items[j].Tag<>1 then
          HistoryList.Items[j].Visible:=false;
      end else begin
        HistoryList.Items[j].Enabled:=true;
        HistoryList.Items[j].Visible:=true;
      end;
    end;
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('delete from historyinfo where id<'+inttostr(i)+';');
  Query1.Open;
end;

procedure TReader.LoadMarkList(LoadType:integer);
var
  no,i:integer;
  s:WideString;
begin
  no:=SystemSets.FileID;
  BookMarks.Clear;
  Query1.Close;
  Query1.SQL.Clear;
  QUery1.SQL.Add('select * from markinfo where id="'+inttostr(no)+'"');
  Query1.Open;
  while not Query1.Eof do
    begin
      no:=Query1.FieldByName('number').value;
      if (no>=1)and(no<=5) then
        begin
          if Query1.FieldByName('position').Value<0 then
            begin
              RMarkList.Items[no-1].Enabled:=false;
              RMarkList.Items[no-1].Caption:='书签&'+inttostr(no);
            end
          else begin
            s:=copy(Read.Text,strtoint(Query1.FieldByName('position').Value),10);
            for i := 1 to length(s) do
              if ord(s[i])<32  then
                s[i]:=WideChar(' ');
            RMarkList.Items[no-1].Caption:='书签&'+inttostr(no)+' '+s;
            RMarkList.Items[no-1].Enabled:=true;
          end;
        end;
      if no=6 then BookMarks.Add(Query1.FieldByName('position').Value);
      Query1.Next;
    end;
  if (LoadType=1) and (MarkF<>nil) then
  begin
    MarkF.MarkLists.Clear;
    for i := 0 to BookMarks.Count - 1 do
      begin
      MarkF.MarkLists.Items.Add(copy(Read.Text,strtoint(BookMarks[i]),20));
      end;
  end;
end;

procedure TReader.MainPopPopup(Sender: TObject);
begin
  if bsBusinessSkinForm1.WindowState=wsMaximized then
    FullScreen1.Caption:='退出全屏(&R)'
  else FullScreen1.Caption:='全屏(&F)';
  Find1.Checked:=FindBar.Visible;
  if OpenDialog1.FileName<>'' then
  begin
    Markit1.Enabled:=True;
    Find1.Enabled:=True;
    Copy1.Enabled:=True;
    G1.Enabled:=True;
    N9.Enabled:=True;
    VoiceSpeak1.Enabled:=True;
    VoiceStop1.Enabled:=True;
    Saveas.Enabled:=True;
  end else begin
    Markit1.Enabled:=False;
    Find1.Enabled:=False;
    Copy1.Enabled:=False;
    G1.Enabled:=False;
    N9.Enabled:=False;
    VoiceSpeak1.Enabled:=False;
    VoiceStop1.Enabled:=False;
    Saveas.Enabled:=False;
  end;
end;

procedure TReader.MainScrollbarChange(Sender: TObject);
begin
  if Read.Pos<>MainScrollBar.Position then
    if MainScrollBar.Position<>MainScrollBar.Max then Read.Pos:=MainScrollBar.Position
      else Read.Pos:=MainScrollBar.Position-1;
end;

procedure TReader.MainScrollbarDownButtonClick(Sender: TObject);
begin
  Read.preLine(1);
end;

procedure TReader.MainScrollbarPageDown(Sender: TObject);
begin
  Read.nextPage;
end;

procedure TReader.MainScrollbarPageUp(Sender: TObject);
begin
  Read.prePage;
end;

procedure TReader.MainScrollbarUpButtonClick(Sender: TObject);
begin
  Read.nextLine(1);
end;

procedure TReader.Memory1Timer(Sender: TObject);
begin
  if Win32Platform = VER_PLATFORM_WIN32_NT then
    begin
      SetProcessWorkingSetSize(GetCurrentProcess, $FFFFFFFF, $FFFFFFFF);
      Application.ProcessMessages;
    end;
end;

procedure TReader.Mouse_Scroll(var Msg: tagMSG; var Handled: Boolean);
var
  i:Smallint;
begin
  if Msg.message = WM_MOUSEWHEEL then
  begin
    Msg.message := WM_KEYDOWN;
    Msg.lParam := 0;
    i:= HiWord(Msg.wParam) ;
    if i > 0 then
      Read.preLine(3) //Msg.wParam := VK_UP
    else
      Read.nextLine(3);//Msg.wParam := VK_DOWN;
    Handled :=false;
  end;
  if Msg.message = WM_KEYDOWN then
  begin
    if Msg.wParam=VK_UP then
      Read.preLine(1);
    if Msg.wParam=VK_DOWN then
      Read.nextLine(1);
    if (Msg.wParam=VK_PRIOR) or (Msg.wParam=VK_LEFT) then //page_up
      Read.prePage;
    if (Msg.wParam=VK_NEXT) or (Msg.wParam=VK_RIGHT) then //page_down
      Read.nextPage;
  end;
end;

procedure TReader.N14Click(Sender: TObject);
var
  MyVoiceSets:TVoiceSets;
begin
  Reader.O1.Click;
  MyVoiceSets:=TVoiceSets.Create(self);
  MyVoiceSets.VoiceList.Items.Clear;
  MyVoiceSets.VoiceList.Items.AddStrings(Reader.Read.VoiceLists);
  MyVoiceSets.VoiceList.ItemIndex:=SystemSets.VoiceIndex;
  MyVoiceSets.rateset.Value:=SystemSets.VoiceRate;
  MyVoiceSets.volset.Value:=SystemSets.VoiceVol;
  MyVoiceSets.ShowModal;
  MyVoiceSets.Free;
end;

procedure TReader.N17Click(Sender: TObject);
begin
  Read.GB:=0;
  Reader.Repaint;
end;

procedure TReader.N2Click(Sender: TObject);
begin
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('delete from historyinfo where id>0');
  Query1.Open;
  LoadHistory;
end;

procedure TReader.N3Click(Sender: TObject);
begin
  CloseFile;
  OpenDialog1.FileName:='';
end;

procedure TReader.N4Click(Sender: TObject);
begin
  OD.InitialDir:=ExtractFilePath(ParamStr(0))+'skins';
  if OD.Execute then
  begin
    bsSkinData1.LoadFromCompressedFile(OD.FileName);
    SystemSets.SkinFile:=OD.FileName;
    N5.Visible:=True;
  end;
end;

procedure TReader.N5Click(Sender: TObject);
begin
  SystemSets.SkinFile:='0';
  bsSkinData1.LoadCompressedStoredSkin(bsCompressedStoredSkin1);
  N5.Visible:=False;
end;

procedure TReader.nextline1Execute(Sender: TObject);
begin
  Read.nextLine(1);
end;

procedure TReader.NextPage1Execute(Sender: TObject);
begin
  Read.nextPage;
end;

procedure TReader.NextParagraphExecute(Sender: TObject);
begin
  Read.nextParagraph;
end;

procedure TReader.Markit1Execute(Sender: TObject);
begin
  SaveMark(6);
end;

procedure TReader.ShowHideClick(Sender: TObject);
begin
  if Visible then
  begin
    if WindowState=wsMinimized then
       WindowState:=wsNormal
    else Visible:=false
  end
  else Visible:=true;
end;

function TReader.OpenFile(Filename: string):boolean;
label 10;
var
  FS:TFileStream;
  TempWString:WideString;
  TempString:String;
  TempAString:AnsiString;
  TempPChar:Pchar;
  fonts:string;
  i,codes:integer;
  Flag:Boolean;   //标记是否插件打开，以确定是否删除
  LibHandle:THandle;
  ReadFile:TReadFile;
begin
  if LastFileName<>'' then
    CloseFile;
  Flag:=True;
  Status.Caption:='正在解码文件,这可能需要一段时间...';
  OpenDialog1.FileName:=LowerCase(OpenDialog1.FileName);
  Filename:=LowerCase(Filename);
  if HeadExt.Count>0 then
    begin
      for i := 0 to HeadExt.Count - 1 do
      begin
        TempString:=copy(HeadExt[i],1,pos('=',HeadExt[i])-1);
        if pos(TempString,Filename)=1 then
        begin
          LibHandle:=LoadLibrary(PChar(HeadExt.Values[TempString]));
          try
            if LibHandle<>0 then
            begin
              @ReadFile:=GetProcAddress(LibHandle,'ReadExtFile');
              if not(@ReadFile=nil) then
                begin
                  TempPChar:=ReadFile(Pchar(Filename));
                  if AnsiString(TempPChar)<>'' then
                  begin
                    Filename:=AnsiString(TempPChar);
                    Flag:=False;
                    break;
                  end;
                end;
            end;
          finally
            FreeLibrary(LibHandle);
          end;
        end;
      end;
    end;
    if (Flag)and(TailExt.Count>0) then
    begin
      for i := 0 to TailExt.Count - 1 do
      begin
        TempString:=copy(TailExt[i],1,pos('=',TailExt[i])-1);
        if pos(TempString,Filename)<>0 then
        begin
          LibHandle:=LoadLibrary(PChar(TailExt.Values[TempString]));
          try
            if LibHandle<>0 then
            begin
              @ReadFile:=GetProcAddress(LibHandle,'ReadExtFile');
              if not(@ReadFile=nil) then
                begin
                  TempPChar:=ReadFile(Pchar(Filename));
                  if AnsiString(TempPChar)<>'' then
                  begin
                    Filename:=AnsiString(TempPChar);
                    Flag:=False;
                    break;
                  end;
                end;
            end;
          finally
            FreeLibrary(LibHandle);
          end;
        end;
      end;
    end;
  try
    FS:=TFileStream.Create(FileName,fmOpenRead);
  except
    result:=false;
    RMessage.MessageDlg('读取文件出错，请确定你有读取此文件的权限，或者该文件正被其它程序使用！',mtError,[mbOK],0);
    exit;
  end;
  codes:=checkCodes(FS);
  if codes=ansi then
  begin
    FS.Seek(0,0);
    SetLength(TempAString,FS.Size);
    FS.Read(TempAString[1],FS.Size);
    TempWString:=WideString(TempAString);
  end else
  if codes=utf8 then
  begin
    FS.Seek(3,0);
    SetLength(TempAString,FS.Size-3);
    FS.Read(TempAString[1],FS.Size-3);
    TempWString:=Utf8Decode(TempAString);
  end else
  if codes=unicode then
  begin
    FS.Seek(2,0);
    SetLength(TempWString,(FS.Size-2) div 2);
    FS.Read(TempWString[1],FS.Size-2);
  end;
  FS.Free;
  //showmessage(inttostr(ord(TempWString[15])));
  if Flag=False then
    DeleteFile(Filename);
  status.Caption:='正在读取书签...';
  BookMarks.Clear;
  10:
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('select * from fileinfo where 文件名="'+OpenDialog1.FileName+'"');
  Query1.Open;
  if not Query1.IsEmpty then begin
  if SystemSets.UseOneFont=False then
  begin
    SystemSets.Fileid:=Query1.FieldByName('id').Value;
    SystemSets.Color:=Query1.FieldByName('背景颜色').Value;
    SystemSets.BgType:=Query1.FieldByName('背景类型').Value;
    SystemSets.BgMode:=Query1.FieldByName('背景模式').Value;
    try
      SystemSets.BgFile:=Query1.FieldByName('背景图片').Value;
    finally
    end;
    StringtoFont(Query1.FieldByName('字体').Value,SystemSets.Font);
  end;
    end else begin
      Query1.Close;
      Query1.SQL.Clear;
      Fonttostring(Systemsets.Font,fonts);
      Query1.SQL.Add('INSERT INTO fileinfo (文件名,背景颜色,背景类型,背景模式,背景图片,字体,作者,大小,小说类型) VALUES("'+openDialog1.Filename+'","'+inttostr(SystemSets.Color)+'","'+booleantostr(SystemSets.BgType)+'","'+inttostr(SystemSets.BgMode)+'","'+SystemSets.BgFile+'","'+fonts+'"," ","'+inttostr(length(TempWString))+'","0")');
      Query1.Open;
      goto 10;
    end;
    Query1.Close;
    Query1.SQL.Clear;
    Query1.SQL.Add('select * from markinfo where id="'+inttostr(SystemSets.FileId)+'"');
    Query1.Open;
    if Query1.IsEmpty then
      for i := 0 to 5 do
        try
          Query1.SQL.Clear;
          Query1.SQL.Add('INSERT INTO markinfo (id,position,number) VALUES("'+inttostr(SystemSets.Fileid)+'","-1","'+inttostr(i)+'")');
          Query1.Open;
        except
        end;
  with SystemSets do begin
    Read.Active:=False;
    Read.Pos:=1;
    Read.Font:=Font;
    Read.Color:=Color;
    Read.BgMode:=BgMode;
    Read.BgFile:=BgFile;
    Read.BgType:=BgType;
    BgFile:=Read.BgFile;
    BgType:=Read.BgType;
    Read.Rows:=RowSpacing;
    Read.Cols:=ListSpacing;
    Read.CrossTime:=CrossTime;
    Read.CrossType:=CrossType;
    Read.AutoCross:=AutoCrossPage;
  end;
  Read.Text:=TempWString;
  Read.Active:=true;
  process1.Max:=Read.Length;
  MainScrollbar.Max:=Read.Length;
  process1.Min:=1;
  MainScrollbar.Min:=1;
  LastFileName:=OpenDialog1.FileName;
  LoadMarkList(0);
  OpenMark(0);
  N3.Enabled:=true;
  MenuButton.Enabled:=True;
  SaveMark1.Enabled:=True;
  SaveMark2.Enabled:=True;
  SaveMark3.Enabled:=True;
  SaveMark4.Enabled:=True;
  SaveMark5.Enabled:=True;
  if Reader.Visible then
  begin
    Query1.Close;
    Query1.SQL.Text:='update systeminfo set 最后打开="'+OpenDialog1.FileName+'" WHERE rowid=1';
    Query1.Open;
  end;
  if SystemSets.AutoFullScreen then
    bsBusinessSkinForm1.WindowState:=wsMaximized;
  status.Caption:='打开文件'+ExtractFileName(OpenDialog1.FileName)+'成功！';
  result:=true;
end;

function TReader.OpenMark(num: integer): boolean;
begin
  Query1.Close;
  Query1.SQL.Clear;
  Query1.SQL.Add('select * from markinfo where id="'+inttostr(SystemSets.FileID)+'" AND number="'+inttostr(num)+'"');
  Query1.Open;
  if not Query1.IsEmpty then
    begin
      Read.Pos:=Query1.FieldByName('position').Value;
    end;
  if num<>0 then
    status.Caption:='打开书签'+inttostr(num)+'成功！';
  result:=true;
end;

procedure TReader.preline1Execute(Sender: TObject);
begin
  read.preLine(1);
end;

procedure TReader.PrePage1Execute(Sender: TObject);
begin
  Read.prePage;
end;

procedure TReader.R11Click(Sender: TObject);
begin
  OpenMark((Sender as TMenuItem).Tag);
end;

procedure TReader.ReadChanged(Sender: TObject);
begin
  process1.Position:=Read.Pos;
  MainScrollBar.Position:=Read.Pos;
end;

procedure TReader.ReadClick(Sender: TObject);
begin
  bsskinedit1.Visible:=false;
end;

procedure TReader.ReadMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Read.AutoCross then
  begin
    CrossPause:=True;
    Read.AutoCross:=False;
  end;
end;

procedure TReader.ReadMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if bsBusinessskinform1.WindowState=wsMaximized then begin
    if y<37 then
      ToolBar.Visible:=true
    else ToolBar.Visible:=false;
    if Height-y<10 then
      bsSkinStatusBar1.Visible:=true
    else bsSkinStatusBar1.Visible:=false;
  end else if y<5 then
    ToolBar.Visible:=true
  else ToolBar.Visible:=false;
  if (SystemSets.ScrollBar)and(Width-X<MainScrollBar.Width) then
    MainScrollBar.Visible:=True
  else MainScrollBar.Visible:=False;
  Read.Tag:=GetTickCount;
  if Screen.Cursor=crNone then Screen.Cursor:=crDefault;
end;

procedure TReader.ReadMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if CrossPause then
  begin
    CrossPause:=False;
    Read.AutoCross:=True;
  end;
end;

procedure TReader.ReadReadComplete(Sender: TObject);
var
  delid:integer;
  FileName:string;
begin
  mouse_event(MOUSEEVENTF_LEFTUP,0,0,0,0);
  if RMessage.MessageDlg('本书已阅读完毕，是否删除本书记录和书签？',mtConfirmation,[mbYES,mbNO],0)=mrYES then
    begin
      delid:=SystemSets.FileID;
      if LastFileName<>'' then    //关闭文件
        Filename:=LastFileName
      else Filename:=OpenDialog1.FileName;
      if FileName<>'' then
        begin
          try
            Query1.Close;
            Query1.SQL.Clear;
            Query1.SQL.Add('delete from historyinfo where 文件名="'+Filename+'";');
            Query1.Open;
          except

          end;
          Read.Active:=false;
          Read.Text:='';
          LastFileName:='';
          SystemSets.FileID:=-1;
          N3.Enabled:=false;
          process1.Max:=2;
          process1.Position:=1;
          MainScrollBar.Max:=2;
          MainScrollBar.Position:=1;
          process2.Caption:='1';
          MenuButton.Enabled:=False;
          SaveMark1.Enabled:=False;
          SaveMark2.Enabled:=False;
          SaveMark3.Enabled:=False;
          SaveMark4.Enabled:=False;
          SaveMark5.Enabled:=False;
          LoadHistory;
          O1.Click;
        end;
      Query1.Close;
      Query1.SQL.Text:='Delete from markinfo where id="'+inttostr(delid)+'";';
      try
        Query1.Open;
      except
      end;
      Query1.SQL.Text:='Delete from fileinfo where id="'+inttostr(delid)+'";';
      try
        Query1.Open;
      except
      end;
      Query1.SQL.Text:='VACUUM';
      Query1.ExecSQL;
  end;
  if RMessage.MessageDlg('打开一部新书？',mtConfirmation,[mbYES,mbNO],0)=mrYES then
    OpenFile1Execute(Sender);
end;

procedure TReader.RebuildDatabaseExecute(Sender: TObject);
begin
  DB1.Close;
  if DeleteFile(DB1.Database) then
  begin
    RepairDataBase;
    DB1.Open;
    RMessage.MessageDlg('重建数据库完成！',mtInformation,[mbOK],0);
  end else
    RMessage.MessageDlg('重建数据库失败！',mtError,[mbOK],0);
end;

procedure TReader.RepairDataBase;
var
  s:string;
begin
  Query1.SQL.Clear;
  Query1.Close;
  s:='CREATE TABLE systeminfo (height NUMERIC, width NUMERIC, x NUMERIC, y NUMERIC, 背景类型 boolean, ';
  s:=s+'背景图片 TEXT, 背景模式 NUMERIC, 背景颜色 NUMERIC, 标题 TEXT, 翻页类型 NUMERIC, 翻页时间 NUMERIC,'+
  ' 列距 NUMERIC, 行距 TEXT, 字体 TEXT, 自动保存书签 boolean, 自动翻页 boolean, 自动清理内存 boolean, '+
  '自动全屏 boolean, 最后打开 TEXT, 皮肤 TEXT, 语音引擎 NUMERIC, 音速 NUMERIC, 音量 NUMERIC, 滚动条 boolean, '+
  '定时提醒 NUMERIC, 统一字体 boolean, 一个实例 boolean);';
  Query1.SQL.Add(s);
  Query1.ExecSQL;
  Query1.SQL.Clear;
  s:='CREATE TABLE systeminfo2 (setname TEXT,value TEXT);';
  Query1.SQL.Add(s);
  Query1.ExecSQL;
  Query1.SQL.Clear;
  s:='CREATE TABLE markinfo (no INTEGER PRIMARY KEY, id NUMERIC, number NUMERIC, position NUMERIC);';
  Query1.SQL.Add(s);
  Query1.ExecSQL;
  Query1.SQL.Clear;
  s:='CREATE TABLE fileinfo (id INTEGER PRIMARY KEY, 文件名 TEXT, 效验码 TEXT, 背景类型 boolean,背景模式 NUMERIC,背景图片 TEXT, 背景颜色 NUMERIC, 字体 TEXT, 小说类型 NUMERIC, 大小 NUMERIC, 作者 TEXT);';
  Query1.SQL.Add(s);
  Query1.ExecSQL;
  Query1.SQL.Clear;
  s:='CREATE TABLE historyinfo (id INTEGER PRIMARY KEY, 文件名 TEXT);';
  Query1.SQL.Add(s);
  Query1.ExecSQL;
  Query1.SQL.Clear;
  s:='CREATE TABLE shortcutinfo (id NUMERIC PRIMARY KEY, ShortCut NUMERIC);';
  Query1.SQL.Add(s);
  Query1.ExecSQL;
  Query1.SQL.Clear;
  s:='insert into systeminfo ';
  s:=s+'(标题,字体,背景颜色,背景模式,背景图片,背景类型,自动清理内存,自动全屏,自动保存书签,自动翻页,翻页类型,翻页时间,行距,列距,x,y,height,width,最后打开,皮肤,语音引擎,音速,音量,滚动条,定时提醒,统一字体,一个实例) ';
  s:=s+'values ("涛儿电子书阅读器","宋体|12|0|0","16708839","1"," ","0","0","0","0","0","2","300","0","2","245","170","452","650"," ","0","0","0","100","0","1","0","1")';
  Query1.SQL.Add(s);
  Query1.ExecSQL;
end;

procedure TReader.RunTimeTimer(Sender: TObject);
var
  i:integer;
begin
  if SystemSets.AwokeHour>0 then
    i:=SystemSets.AwokeHour
  else i:=1;
  (Sender as TTimer).Tag:=(Sender as TTimer).Tag + 1;
  case ClearStatus.Tag of
    2:status.Caption:='现在时间：'+Timetostr(now);
    3:status.Caption:='目前已经阅读时间：'+inttostr(Runtime.Tag div 3600)+'小时'+inttostr(Runtime.Tag mod 3600 div 60)+'分'+inttostr(Runtime.Tag mod 60)+'秒';
  end;
  if (Sender as TTimer).Tag mod (i*3600)=0 then
    RMessage.MessageDlg('您已经使用本软件阅读了'+inttostr((Sender as TTimer).Tag div 3600)+'小时，请稍微休息下，注意保护好视力！',mtInformation,[mbOK],0);
  if bsBusinessSkinForm1.WindowState=wsMaximized then
  begin
    if GetTickCount-LongWord(Read.Tag)>3000 then
    begin
      if Reader.Active then Screen.Cursor:=crNone
      else Screen.Cursor:=crDefault;
    end;
  end;
end;

procedure TReader.SaveasExecute(Sender: TObject);
var
  SSL:TStringList;
begin
  if SaveDialog1.Execute then
  begin
    SSL:=TStringList.Create;
    SSL.Text:=Read.Text;
    SSL.SaveToFile(SaveDialog1.FileName);
  end;
end;

function TReader.SaveMark(num: integer): boolean;
var
  Filename:String;
begin
  if LastFileName<>'' then
    Filename:=LastFileName
  else Filename:=Opendialog1.FileName;
  if Filename='' then
  begin
    result:=false;
    exit;
  end;
  if num<=5 then begin
    Query1.Close;
    Query1.SQL.Clear;
    Query1.SQL.Add('UPDATE markinfo SET position = "'+inttostr(Read.Pos)+'" where number="'+inttostr(num)+'" and id="'+inttostr(SystemSets.FileID)+'"');
    Query1.Open;
  end;
  if num=6 then begin
    Query1.Close;
    Query1.SQL.Clear;
    Query1.SQL.Add('INSERT into markinfo (position,number,id) values ("'+inttostr(Read.Pos)+'","'+inttostr(num)+'","'+inttostr(SystemSets.FileID)+'")');
    Query1.Open;
  end;
  if num=6 then LoadMarkList(1) else LoadMarkList(0);
  if num=6 then
    status.Caption:='标记成功！'
  else if (num>=1)and(num<=5) then
    status.Caption:='保存书签'+inttostr(num)+'成功！';
  result:=true;
end;

procedure TReader.SaveMark1Execute(Sender: TObject);
begin
  SaveMark((Sender as TAction).Tag);
end;

procedure TReader.StringtoFont(S: string; var Font: TFont);
var
  tempstrings:TStringList;
  i:integer;
begin
  tempstrings:=TStringList.Create;
  tempstrings.Delimiter:='|';
  tempstrings.DelimitedText:=s;
  Font.Name:=tempstrings[0];
  Font.Size:=strtoint(tempstrings[1]);
  i:=strtoint(tempstrings[2]);
  Font.Color:=strtoint(tempstrings[3]);
  tempstrings.Free;
  Font.Style:=[];
  if i and 1=1 then
    Font.Style:=Font.Style+[fsBold];
  if (i shr 1) and 1=1 then
    Font.Style:=Font.Style+[fsItalic];
  if (i shr 2) and 1=1 then
    Font.Style:=Font.Style+[fsUnderline];
  if (i shr 3) and 1=1 then
    Font.Style:=Font.Style+[fsStrikeOut];
end;

procedure TReader.TraypopPopup(Sender: TObject);
begin
  if Visible then
  begin
    if WindowState=wsMinimized then
      ShowHide.Caption:='还原(&R)'
    else ShowHide.Caption:='隐藏(&H)';
  end
  else ShowHide.Caption:='显示(&S)';
end;

procedure TReader.updateDatabase;
begin
  //判断表systeminfo2是否存在
  Query1.SQL.Text:='select * from sqlite_master where tbl_name="systeminfo2";';
  Query1.Open;
  if Query1.IsEmpty then
  begin
    Query1.SQL.Text:='CREATE TABLE systeminfo2 (setname TEXT,value TEXT);';
    Query1.ExecSQL;
  end;
end;

procedure TReader.UpdateMarkList;
var
  i:integer;
begin
  if SystemSets.FileID>-1 then
  begin
    Query1.Close;
    Query1.SQL.Clear;
    Query1.SQL.Add('Delete from markinfo where id="'+inttostr(SystemSets.FileID)+'" and number="6"');
    try
      Query1.Open;
    except
    end;
    Query1.Close;
    Query1.SQL.Text:='BEGIN;';
    Query1.ExecSQL;
    for i := 0 to BookMarks.Count - 1 do
    begin
      Query1.SQL.Text:='insert into markinfo (position,number,id) values ("'+BookMarks[i]+'","6","'+inttostr(SystemSets.FileID)+'");';
      Query1.ExecSQL;
    end;
    Query1.SQL.Text:='COMMIT;';
    Query1.ExecSQL;
  end;
  LoadMarkList(1);
end;

procedure TReader.VoiceSpeak1Execute(Sender: TObject);
begin
  Read.Speak;
end;

procedure TReader.VoiceSpeedAddExecute(Sender: TObject);
begin
  if SpVoice1.Rate<10 then
  begin
    SystemSets.VoiceRate:=SystemSets.VoiceRate+1;
    SpVoice1.Rate:=SpVoice1.Rate+1;
  end;
end;

procedure TReader.VoiceSpeedMulExecute(Sender: TObject);
begin
  if SpVoice1.Rate>-10 then
  begin
    SystemSets.VoiceRate:=SystemSets.VoiceRate-1;
    SpVoice1.Rate:=SpVoice1.Rate-1;
  end;
end;

procedure TReader.WMDROPFILES(var Message: TWMDROPFILES);
var
  buffer : array[0..255] of char;
begin
  DragQueryFile(Message.Drop,0,@buffer,sizeof(buffer));
  OpenDialog1.FileName:=buffer;
  OpenFile(buffer);
end;

procedure TReader.WMHotKey(var Msg: TWMHotKey);
begin
  //if Msg.HotKey = bosskey then Reader.Visible:=not Reader.Visible;
  if Msg.HotKey = bosskey then
    if Reader.Visible then
    begin
      WindowState:=wsMinimized;
      Reader.Visible:=False;
    end else begin
      Reader.Visible:=True;
      WindowState:=wsNormal;
    end;
end;

procedure TReader.WMMOVE(var Msg: TMessage);
begin
  Inherited;
  if (Reader.Visible) and (FileManager<>nil) and (FileManager.Visible) then
  begin
    FileManager.Left:=Reader.Left - FileManager.Width;
    FileManager.Top:=Reader.Top;
    FileManager.Height:=Reader.Height;
  end;
  if (Reader.Visible) and (MarkF<>nil) and (MarkF.Visible) then
  begin
    if FileManager.Visible then MarkF.Left:=FileManager.Left - MarkF.Width
      else MarkF.left :=Reader.Left - MarkF.width;
    MarkF.top:=Reader.Top;
    MarkF.height:=Reader.Height;
  end;
end;

procedure TReader.OpenFile1Execute(Sender: TObject);
label 10;
begin
  10:
  status.caption:='正在打开文件...';
  if OpenDialog1.Execute then
    begin
      if not OpenFile(OpenDialog1.FileName) then goto 10;
    end else
    begin
      status.OnClick(status);
      OpenDialog1.FileName:=LastFilename;
    end;
end;

end.
