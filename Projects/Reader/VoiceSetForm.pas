unit VoiceSetForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BusinessSkinForm, StdCtrls, Mask, bsSkinBoxCtrls, bsSkinCtrls,
  bsSkinExCtrls;

type
  TVoiceSets = class(TForm)
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    bsSkinRadioGroup1: TbsSkinRadioGroup;
    VoiceList: TbsSkinComboBox;
    bsSkinLabel1: TbsSkinLabel;
    bsSkinLabel2: TbsSkinLabel;
    rateset: TbsSkinTrackEdit;
    volset: TbsSkinTrackEdit;
    Sure: TbsSkinButton;
    Cancel: TbsSkinButton;
    bsSkinPanel1: TbsSkinPanel;
    VoiceInfo: TbsSkinTextLabel;
    goDown: TbsSkinLinkLabel;
    TestText: TbsSkinEdit;
    play: TbsSkinButton;
    procedure CancelClick(Sender: TObject);
    procedure VoiceListChange(Sender: TObject);
    procedure SureClick(Sender: TObject);
    procedure playClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  VoiceSets: TVoiceSets;

implementation
uses MainForm,publicvar;

{$R *.dfm}

procedure TVoiceSets.CancelClick(Sender: TObject);
begin
  Reader.Read.VoiceRate:=SystemSets.VoiceRate;
  Reader.Read.VoiceVol:=SystemSets.VoiceVol;
  Reader.Read.VoiceIndex:=SystemSets.VoiceIndex;
  Close;
end;

procedure TVoiceSets.playClick(Sender: TObject);
begin
  Reader.SpVoice1.Rate:=Rateset.Value;
  Reader.SpVoice1.Volume:=VolSet.Value;
  Reader.SpVoice1.Speak(WideString(TestText.Text), $00000001);
end;

procedure TVoiceSets.SureClick(Sender: TObject);
begin
  SystemSets.VoiceIndex:=VoiceList.ItemIndex;
  SystemSets.VoiceRate:=RateSet.Value;
  SystemSets.VoiceVol:=VolSet.Value;
  Reader.Read.VoiceIndex:=SystemSets.VoiceIndex;
  Reader.Read.VoiceRate:=SystemSets.VoiceRate;
  Reader.Read.VoiceVol:=SystemSets.VoiceVol;
  Close;
end;

procedure TVoiceSets.VoiceListChange(Sender: TObject);
begin
  Reader.Read.VoiceIndex:=VoiceList.ItemIndex;
  VoiceInfo.Lines.Clear;
  VoiceInfo.Lines.Add(Reader.Read.VoiceInformation);
end;

end.
