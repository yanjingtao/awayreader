object FileManager: TFileManager
  Left = 219
  Top = 143
  BorderIcons = []
  Caption = #20070#31821#31649#29702
  ClientHeight = 431
  ClientWidth = 208
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BookTypeList: TbsSkinComboBox
    Left = 0
    Top = 0
    Width = 208
    Height = 20
    HintImageIndex = 0
    TabOrder = 1
    SkinData = Reader.bsSkinData1
    SkinDataName = 'combobox'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = 14
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultWidth = 0
    DefaultHeight = 0
    UseSkinFont = False
    UseSkinSize = True
    ToolButtonStyle = False
    AlphaBlend = False
    AlphaBlendValue = 0
    AlphaBlendAnimation = False
    ListBoxCaptionMode = False
    ListBoxDefaultFont.Charset = DEFAULT_CHARSET
    ListBoxDefaultFont.Color = clWindowText
    ListBoxDefaultFont.Height = 14
    ListBoxDefaultFont.Name = 'Arial'
    ListBoxDefaultFont.Style = []
    ListBoxDefaultCaptionFont.Charset = DEFAULT_CHARSET
    ListBoxDefaultCaptionFont.Color = clWindowText
    ListBoxDefaultCaptionFont.Height = 14
    ListBoxDefaultCaptionFont.Name = 'Arial'
    ListBoxDefaultCaptionFont.Style = []
    ListBoxDefaultItemHeight = 20
    ListBoxCaptionAlignment = taLeftJustify
    ListBoxUseSkinFont = True
    ListBoxUseSkinItemHeight = True
    ListBoxWidth = 0
    HideSelection = True
    AutoComplete = True
    ImageIndex = -1
    CharCase = ecNormal
    DefaultColor = clWindow
    Text = #20840#37096#31867#22411
    Align = alTop
    Items.Strings = (
      #20840#37096#31867#22411
      #29572#24187#22855#24187
      #27494#20384#20185#20384
      #37117#24066#35328#24773
      #21382#21490#20891#20107
      #28216#25103#31454#25216
      #31185#24187#28789#24322
      #32829#32654#21516#20154
      #20854#23427#31867#22411)
    ItemIndex = 0
    DropDownCount = 8
    HorizontalExtent = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = 14
    Font.Name = 'Arial'
    Font.Style = []
    Sorted = False
    Style = bscbFixedStyle
    OnChange = BookTypeListChange
  end
  object BookList: TbsSkinListBox
    Left = 0
    Top = 20
    Width = 208
    Height = 358
    HintImageIndex = 0
    TabOrder = 0
    SkinData = Reader.bsSkinData1
    SkinDataName = 'listbox'
    DefaultFont.Charset = GB2312_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -12
    DefaultFont.Name = #26032#23435#20307
    DefaultFont.Style = []
    DefaultWidth = 0
    DefaultHeight = 0
    UseSkinFont = False
    AutoComplete = True
    UseSkinItemHeight = True
    HorizontalExtent = False
    Columns = 0
    RowCount = 0
    ImageIndex = -1
    NumGlyphs = 1
    Spacing = 2
    CaptionMode = False
    DefaultCaptionHeight = 20
    DefaultCaptionFont.Charset = GB2312_CHARSET
    DefaultCaptionFont.Color = clWindowText
    DefaultCaptionFont.Height = -12
    DefaultCaptionFont.Name = #26032#23435#20307
    DefaultCaptionFont.Style = []
    DefaultItemHeight = 20
    ItemIndex = -1
    MultiSelect = False
    ListBoxFont.Charset = GB2312_CHARSET
    ListBoxFont.Color = clWindowText
    ListBoxFont.Height = -12
    ListBoxFont.Name = #26032#23435#20307
    ListBoxFont.Style = []
    ListBoxTabOrder = 0
    ListBoxTabStop = True
    ListBoxDragMode = dmManual
    ListBoxDragKind = dkDrag
    ListBoxDragCursor = crDrag
    ExtandedSelect = False
    Sorted = False
    ShowCaptionButtons = True
    Font.Charset = GB2312_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = #26032#23435#20307
    Font.Style = []
    Align = alClient
    ParentShowHint = False
    ShowHint = True
    OnListBoxClick = BookListListBoxClick
    OnListBoxDblClick = BookListListBoxDblClick
  end
  object StatusBar: TbsSkinStatusBar
    Left = 0
    Top = 410
    Width = 208
    Height = 21
    HintImageIndex = 0
    TabOrder = 2
    SkinData = Reader.bsSkinData1
    SkinDataName = 'statusbar'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = 14
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultWidth = 0
    DefaultHeight = 0
    UseSkinFont = True
    ImagePosition = bsipDefault
    TransparentMode = False
    CaptionImageIndex = -1
    RealHeight = -1
    AutoEnabledControls = True
    CheckedMode = False
    Checked = False
    DefaultAlignment = taLeftJustify
    DefaultCaptionHeight = 22
    BorderStyle = bvNone
    CaptionMode = False
    RollUpMode = False
    RollUpState = False
    NumGlyphs = 1
    Spacing = 2
    Caption = 'StatusBar'
    Align = alBottom
    SizeGrip = False
    object StatusPanel1: TbsSkinStatusPanel
      Left = 0
      Top = 0
      Width = 156
      Height = 21
      HintImageIndex = 0
      TabOrder = 0
      SkinData = Reader.bsSkinData1
      SkinDataName = 'statuspanel'
      DefaultFont.Charset = DEFAULT_CHARSET
      DefaultFont.Color = clWindowText
      DefaultFont.Height = 14
      DefaultFont.Name = 'Arial'
      DefaultFont.Style = []
      DefaultWidth = 0
      DefaultHeight = 0
      UseSkinFont = True
      ShadowEffect = False
      ShadowColor = clBlack
      ShadowOffset = 0
      ShadowSize = 3
      ReflectionEffect = False
      ReflectionOffset = -5
      EllipsType = bsetNoneEllips
      UseSkinSize = True
      UseSkinFontColor = True
      BorderStyle = bvFrame
      Align = alLeft
      AutoSize = False
      ImageIndex = -1
      NumGlyphs = 1
    end
  end
  object Panel1: TbsSkinPanel
    Left = 0
    Top = 378
    Width = 208
    Height = 32
    HintImageIndex = 0
    TabOrder = 3
    SkinData = Reader.bsSkinData1
    SkinDataName = 'panel'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = 14
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultWidth = 0
    DefaultHeight = 0
    UseSkinFont = True
    ImagePosition = bsipDefault
    TransparentMode = False
    CaptionImageIndex = -1
    RealHeight = -1
    AutoEnabledControls = True
    CheckedMode = False
    Checked = False
    DefaultAlignment = taLeftJustify
    DefaultCaptionHeight = 22
    BorderStyle = bvFrame
    CaptionMode = False
    RollUpMode = False
    RollUpState = False
    NumGlyphs = 1
    Spacing = 2
    Caption = 'Panel1'
    Align = alBottom
    object BtnAddBook: TbsSkinButton
      Left = 3
      Top = 3
      Width = 46
      Height = 25
      Hint = #28155#21152#19968#20010#25991#20214#25110#32773#30446#24405
      HintImageIndex = 0
      TabOrder = 0
      SkinData = Reader.bsSkinData1
      SkinDataName = 'button'
      DefaultFont.Charset = DEFAULT_CHARSET
      DefaultFont.Color = clWindowText
      DefaultFont.Height = 14
      DefaultFont.Name = 'Arial'
      DefaultFont.Style = []
      DefaultWidth = 0
      DefaultHeight = 0
      UseSkinFont = False
      ImageIndex = -1
      AlwaysShowLayeredFrame = False
      UseSkinSize = True
      UseSkinFontColor = True
      RepeatMode = False
      RepeatInterval = 100
      AllowAllUp = False
      ShowHint = True
      TabStop = True
      CanFocused = True
      ParentShowHint = False
      Down = False
      GroupIndex = 0
      Caption = #28155#21152
      NumGlyphs = 1
      Spacing = 1
      OnClick = BtnAddBookClick
    end
    object BtnModifyBook: TbsSkinButton
      Left = 56
      Top = 3
      Width = 46
      Height = 25
      Hint = #20462#25913#36873#20013#39033
      HintImageIndex = 0
      TabOrder = 1
      SkinData = Reader.bsSkinData1
      SkinDataName = 'button'
      DefaultFont.Charset = DEFAULT_CHARSET
      DefaultFont.Color = clWindowText
      DefaultFont.Height = 14
      DefaultFont.Name = 'Arial'
      DefaultFont.Style = []
      DefaultWidth = 0
      DefaultHeight = 0
      UseSkinFont = False
      ImageIndex = -1
      AlwaysShowLayeredFrame = False
      UseSkinSize = True
      UseSkinFontColor = True
      RepeatMode = False
      RepeatInterval = 100
      AllowAllUp = False
      ShowHint = True
      TabStop = True
      CanFocused = True
      ParentShowHint = False
      Down = False
      GroupIndex = 0
      Caption = #20462#25913
      NumGlyphs = 1
      Spacing = 1
      Enabled = False
      OnClick = BtnModifyBookClick
    end
    object BtnDeleteBook: TbsSkinButton
      Left = 108
      Top = 3
      Width = 46
      Height = 25
      Hint = #21024#38500#36873#20013#39033
      HintImageIndex = 0
      TabOrder = 2
      SkinData = Reader.bsSkinData1
      SkinDataName = 'button'
      DefaultFont.Charset = DEFAULT_CHARSET
      DefaultFont.Color = clWindowText
      DefaultFont.Height = 14
      DefaultFont.Name = 'Arial'
      DefaultFont.Style = []
      DefaultWidth = 0
      DefaultHeight = 0
      UseSkinFont = False
      ImageIndex = -1
      AlwaysShowLayeredFrame = False
      UseSkinSize = True
      UseSkinFontColor = True
      RepeatMode = False
      RepeatInterval = 100
      AllowAllUp = False
      ShowHint = True
      TabStop = True
      CanFocused = True
      ParentShowHint = False
      Down = False
      GroupIndex = 0
      Caption = #21024#38500
      NumGlyphs = 1
      Spacing = 1
      Enabled = False
      OnClick = BtnDeleteBookClick
    end
    object BtnRefresh: TbsSkinButton
      Left = 160
      Top = 3
      Width = 45
      Height = 25
      Hint = #21047#26032#21015#34920
      HintImageIndex = 0
      TabOrder = 3
      SkinData = Reader.bsSkinData1
      SkinDataName = 'button'
      DefaultFont.Charset = DEFAULT_CHARSET
      DefaultFont.Color = clWindowText
      DefaultFont.Height = 14
      DefaultFont.Name = 'Arial'
      DefaultFont.Style = []
      DefaultWidth = 0
      DefaultHeight = 0
      UseSkinFont = True
      ImageIndex = -1
      AlwaysShowLayeredFrame = False
      UseSkinSize = True
      UseSkinFontColor = True
      RepeatMode = False
      RepeatInterval = 100
      AllowAllUp = False
      ShowHint = True
      TabStop = True
      CanFocused = True
      ParentShowHint = False
      Down = False
      GroupIndex = 0
      Caption = #21047#26032
      NumGlyphs = 1
      Spacing = 1
      OnClick = BtnRefreshClick
    end
  end
  object bsBusinessSkinForm1: TbsBusinessSkinForm
    ClientInActiveEffect = False
    ClientInActiveEffectType = bsieSemiTransparent
    DisableSystemMenu = False
    AlwaysResize = False
    PositionInMonitor = bspDefault
    UseFormCursorInNCArea = False
    MaxMenuItemsInWindow = 0
    ClientWidth = 0
    ClientHeight = 0
    HideCaptionButtons = False
    AlwaysShowInTray = False
    LogoBitMapTransparent = False
    AlwaysMinimizeToTray = False
    UseSkinFontInMenu = False
    ShowIcon = False
    MaximizeOnFullScreen = False
    AlphaBlend = False
    AlphaBlendAnimation = False
    AlphaBlendValue = 200
    ShowObjectHint = False
    MenusAlphaBlend = False
    MenusAlphaBlendAnimation = False
    MenusAlphaBlendValue = 200
    DefCaptionFont.Charset = DEFAULT_CHARSET
    DefCaptionFont.Color = clBtnText
    DefCaptionFont.Height = 14
    DefCaptionFont.Name = 'Arial'
    DefCaptionFont.Style = [fsBold]
    DefInActiveCaptionFont.Charset = DEFAULT_CHARSET
    DefInActiveCaptionFont.Color = clBtnShadow
    DefInActiveCaptionFont.Height = 14
    DefInActiveCaptionFont.Name = 'Arial'
    DefInActiveCaptionFont.Style = [fsBold]
    DefMenuItemHeight = 20
    DefMenuItemFont.Charset = DEFAULT_CHARSET
    DefMenuItemFont.Color = clWindowText
    DefMenuItemFont.Height = 14
    DefMenuItemFont.Name = 'Arial'
    DefMenuItemFont.Style = []
    UseDefaultSysMenu = True
    SkinData = Reader.bsSkinData1
    MenusSkinData = Reader.bsSkinData1
    MinHeight = 100
    MinWidth = 172
    MaxHeight = 0
    MaxWidth = 0
    Magnetic = False
    MagneticSize = 5
    BorderIcons = []
    Left = 16
    Top = 16
  end
  object AddMenu: TbsSkinPopupMenu
    SkinData = Reader.bsSkinData1
    Left = 8
    Top = 324
    object F1: TMenuItem
      Caption = #28155#21152#19968#20010#25991#20214'(&F)...'
      OnClick = F1Click
    end
    object M1: TMenuItem
      Caption = #28155#21152#19968#20010#30446#24405'(&M)...'
      Hint = #36873#25321#19968#20010#30446#24405#36827#34892#28155#21152#65292#21487#20197#35774#32622#20026#30417#35270#30446#24405#12290#13#13#36873#25321#25105#30340#30005#33041#65292#21487#20197#35774#32622#21462#28040#30417#35270#30446#24405#21151#33021#12290
      OnClick = M1Click
    end
  end
  object Query1: TASQLite3Query
    AutoCommit = False
    SQLiteDateFormat = True
    Connection = Reader.DB1
    MaxResults = 0
    StartResult = 0
    TypeLess = False
    SQLCursor = True
    ReadOnly = False
    UniDirectional = False
    RawSQL = False
    Left = 48
    Top = 16
  end
  object SelectDirectoryDialog1: TbsSkinSelectDirectoryDialog
    ToolButtonsTransparent = False
    DialogWidth = 0
    DialogHeight = 0
    DialogMinWidth = 0
    DialogMinHeight = 0
    AlphaBlend = False
    AlphaBlendValue = 200
    AlphaBlendAnimation = False
    SkinData = Reader.bsSkinData1
    CtrlSkinData = Reader.bsSkinData1
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = 14
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    Title = #36873#25321#38656#35201#28155#21152#30340#30446#24405
    ShowToolBar = False
    Left = 48
    Top = 324
  end
  object Query2: TASQLite3Query
    AutoCommit = False
    SQLiteDateFormat = True
    Connection = Reader.DB1
    MaxResults = 0
    StartResult = 0
    TypeLess = False
    SQLCursor = True
    ReadOnly = False
    UniDirectional = False
    RawSQL = False
    Left = 152
    Top = 16
  end
end
