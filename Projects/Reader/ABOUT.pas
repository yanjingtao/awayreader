unit About;

interface

uses Windows, SysUtils, Classes, Graphics, Forms, Controls, StdCtrls,
  Buttons, ExtCtrls, bsSkinCtrls, BusinessSkinForm, bsSkinExCtrls, publicvar;

type
  TAboutBox = class(TForm)
    Panel1: TbsskinPanel;
    ProgramIcon: TImage;
    ProductName: TLabel;
    Version: TLabel;
    Copyright: TLabel;
    Comments: TLabel;
    OKButton: TbsskinButton;
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    bsSkinLinkLabel1: TbsSkinLinkLabel;
    bsSkinLinkLabel2: TbsSkinLinkLabel;
    bsSkinLinkLabel3: TbsSkinLinkLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AboutBox: TAboutBox;

implementation

uses MainForm;

{$R *.dfm}

procedure TAboutBox.FormCreate(Sender: TObject);
begin
  Version.Caption := Reader_version;
end;

end.
 
