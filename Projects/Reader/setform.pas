unit setform;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BusinessSkinForm, bsSkinCtrls, bsSkinBoxCtrls, bsColorCtrls,
  bsDialogs, StdCtrls, Mask, bsSkinShellCtrls;

type
  Tsetting = class(TForm)
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    bsSkinGroupBox1: TbsSkinGroupBox;
    b1: TbsSkinCheckRadioBox;
    b2: TbsSkinCheckRadioBox;
    color1: TbsSkinColorButton;
    font1: TbsSkinFontDialog;
    bsSkinButton1: TbsSkinButton;
    file1: TbsSkinFileEdit;
    bsSkinGroupBox2: TbsSkinGroupBox;
    bsSkinGroupBox3: TbsSkinGroupBox;
    s1: TbsSkinCheckRadioBox;
    s3: TbsSkinCheckRadioBox;
    s2: TbsSkinCheckRadioBox;
    s4: TbsSkinCheckRadioBox;
    fylx: TbsSkinComboBox;
    fysj: TbsSkinSpinEdit;
    bsSkinLabel1: TbsSkinLabel;
    sjtitle: TbsSkinLabel;
    bsSkinButton2: TbsSkinButton;
    bsSkinButton3: TbsSkinButton;
    bsSkinButton4: TbsSkinButton;
    bsSkinLabel3: TbsSkinLabel;
    bsSkinLabel4: TbsSkinLabel;
    hj: TbsSkinSpinEdit;
    lj: TbsSkinSpinEdit;
    OpenPicture: TbsSkinOpenPictureDialog;
    bgmode: TbsSkinComboBox;
    VoiceSet: TbsSkinButton;
    ScrollBars: TbsSkinCheckRadioBox;
    txsj: TbsSkinSpinEdit;
    bsSkinStdLabel1: TbsSkinStdLabel;
    UseOneFont: TbsSkinCheckRadioBox;
    bsSkinButton5: TbsSkinButton;
    OnyOne: TbsSkinCheckRadioBox;
    bsSkinGroupBox4: TbsSkinGroupBox;
    Mleft: TbsSkinNumericEdit;
    Mright: TbsSkinNumericEdit;
    Mtop: TbsSkinNumericEdit;
    Mbottom: TbsSkinNumericEdit;
    bsSkinStdLabel2: TbsSkinStdLabel;
    bsSkinStdLabel3: TbsSkinStdLabel;
    bsSkinStdLabel4: TbsSkinStdLabel;
    bsSkinStdLabel5: TbsSkinStdLabel;
    procedure bsSkinButton1Click(Sender: TObject);
    procedure b1Click(Sender: TObject);
    procedure s4Click(Sender: TObject);
    procedure bsSkinButton4Click(Sender: TObject);
    procedure bsSkinButton3Click(Sender: TObject);
    procedure bsSkinButton2Click(Sender: TObject);
    procedure fylxChange(Sender: TObject);
    procedure file1ButtonClick(Sender: TObject);
    procedure VoiceSetClick(Sender: TObject);
    procedure bsSkinButton5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  setting: Tsetting;

implementation
uses publicvar,MainForm,VoiceSetForm,ShortCutForm;

{$R *.dfm}

procedure Tsetting.b1Click(Sender: TObject);
begin
  if b1.Checked then
    begin
      color1.Enabled:=true;
      file1.Enabled:=false;
      bgmode.Enabled:=false;
    end
    else begin
      color1.Enabled:=false;
      file1.Enabled:=true;
      bgmode.Enabled:=true;
    end;
end;

procedure Tsetting.bsSkinButton1Click(Sender: TObject);
begin
  font1.Execute;
end;

procedure Tsetting.bsSkinButton2Click(Sender: TObject);
begin
  SystemSets.Font.Name:=Font1.Font.Name;
  SystemSets.Font.Size:=Font1.Font.Size;
  SystemSets.Font.Style:=[];
  if fsBold in Font1.Font.Style then
    SystemSets.Font.Style:=SystemSets.Font.Style+[fsBold];
  if fsItalic in Font1.Font.Style then
    SystemSets.Font.Style:=SystemSets.Font.Style+[fsItalic];
  if fsUnderline in Font1.Font.Style then
    SystemSets.Font.Style:=SystemSets.Font.Style+[fsUnderline];
  if fsStrikeOut in Font1.Font.Style then
    SystemSets.Font.Style:=SystemSets.Font.Style+[fsStrikeOut];
  SystemSets.Font.Color:=Font1.Font.Color;
  SystemSets.Color:=Color1.ColorValue;
  SystemSets.RowSpacing:=strtoint(hj.Text);
  SystemSets.ListSpacing:=strtoint(lj.Text);
  SystemSets.BgType:=b2.Checked;
  SystemSets.BgFile:=file1.Text;
  SystemSets.BgMode:=bgmode.ItemIndex;
  SystemSets.Memory:=s1.Checked;
  SystemSets.AutoFullScreen:=s2.Checked;
  SystemSets.AutoSaveMark:=s3.Checked;
  SystemSets.AutoCrossPage:=s4.Checked;
  SystemSets.ScrollBar:=ScrollBars.Checked;
  SystemSets.RowSpacing:=round(hj.Value);
  SystemSets.ListSpacing:=round(lj.Value);
  SystemSets.CrossType:=fylx.ItemIndex;
  SystemSets.CrossTime:=round(fysj.Value);
  SystemSets.AwokeHour:=round(txsj.Value);
  SystemSets.UseOneFont:=UseOneFont.Checked;
  Reader.Memory1.Enabled:=SystemSets.Memory;
  Reader.AutoMark1.Enabled:=SystemSets.AutoSaveMark;
  Reader.Read.CrossTime:=SystemSets.CrossTime;
  Reader.Read.CrossType:=SystemSets.CrossType;
  Reader.Read.AutoCross:=SystemSets.AutoCrossPage;
  Reader.Read.Color:=SystemSets.Color;
  Reader.Read.BgFile:=SystemSets.BgFile;
  Reader.Read.BgMode:=SystemSets.BgMode;
  Reader.Read.BgType:=SystemSets.BgType;
  SystemSets.BgFile:=Reader.Read.BgFile;
  SystemSets.BgType:=Reader.Read.BgType;
  SystemSets.OnyOne:=OnyOne.Checked;
  SystemSets.MarginLeft:=round(MLeft.Value);
  SystemSets.MarginRight:=round(MRight.Value);
  SystemSets.MarginTop:=round(MTop.Value);
  SystemSets.MarginBottom:=round(MBottom.Value);
  Reader.Read.Font:=SystemSets.Font;
  Reader.Read.Cols:=SystemSets.ListSpacing;
  Reader.Read.Rows:=SystemSets.RowSpacing;
  Reader.MainScrollbar.Visible:=SystemSets.ScrollBar;
  Reader.Read.Margins.Left:=SystemSets.MarginLeft;
  Reader.Read.Margins.Right:=SystemSets.MarginRight;
  Reader.Read.Margins.Top:=SystemSets.MarginTop;
  Reader.Read.Margins.Bottom:=SystemSets.MarginBottom;
  if Reader.Read.Active then
    Reader.Read.Refresh;
end;

procedure Tsetting.bsSkinButton3Click(Sender: TObject);
begin
  bsSkinButton2Click(Sender);
  close;
end;

procedure Tsetting.bsSkinButton4Click(Sender: TObject);
begin
  close;
end;

procedure Tsetting.bsSkinButton5Click(Sender: TObject);
var
  SSC:TSetShortCut;
begin
  SSC:=TSetShortCut.Create(self);
  SSC.ShowModal;
  SSC.Free;
end;

procedure Tsetting.file1ButtonClick(Sender: TObject);
begin
  try
  OpenPicture.FileName:=file1.Text;
  if OpenPicture.Execute then
    file1.Text:=OpenPicture.FileName;
  except
    OpenPicture.filename:=ExtractFilePath(Paramstr(0));
    if OpenPicture.Execute then
      file1.Text:=OpenPicture.FileName;
  end;
end;

procedure Tsetting.FormCreate(Sender: TObject);
begin
  try
  font1.Font.Name:=SystemSets.Font.Name;
  font1.Font.Size:=SystemSets.Font.Size;
  font1.Font.Style:=[];
  if fsBold in SystemSets.Font.Style then
    font1.Font.Style:=font1.Font.Style+[fsBold];
  if fsItalic in SystemSets.Font.Style then
    font1.Font.Style:=font1.Font.Style+[fsItalic];
  if fsUnderline in SystemSets.Font.Style then
    font1.Font.Style:=font1.Font.Style+[fsUnderline];
  if fsStrikeOut in SystemSets.Font.Style then
    font1.Font.Style:=font1.Font.Style+[fsStrikeOut];
  font1.Font.Color:=SystemSets.Font.Color;
  color1.ColorValue:=SystemSets.Color;
  hj.Text:=inttostr(SystemSets.RowSpacing);
  lj.Text:=inttostr(SystemSets.ListSpacing);
  if SystemSets.BgType then
  begin
    b2.Checked:=true;
    b2.OnClick(b2);
  end
  else begin
    b1.Checked:=true;
    b1.OnClick(b1);
  end;
  Mleft.Value:=SystemSets.MarginLeft;
  Mright.Value:=SystemSets.MarginRight;
  Mtop.Value:=SystemSets.MarginTop;
  Mbottom.Value:=SystemSets.MarginBottom;
  file1.Text:=SystemSets.BgFile;
  bgmode.ItemIndex:=SystemSets.BgMode;
  s1.Checked:=SystemSets.Memory;
  s2.Checked:=SystemSets.AutoFullScreen;
  s3.Checked:=SystemSets.AutoSaveMark;
  s4.Checked:=SystemSets.AutoCrossPage;
  txsj.Value:=SystemSets.AwokeHour;
  UseOneFont.Checked:=SystemSets.UseOneFont;
  ScrollBars.Checked:=SystemSets.ScrollBar;
  hj.Value:=SystemSets.RowSpacing;
  lj.Value:=SystemSets.ListSpacing;
  fylx.ItemIndex:=SystemSets.CrossType;
  fylx.OnChange(fylx);
  fysj.Value:=SystemSets.CrossTime;
  fylx.Enabled:=s4.Checked;
  fysj.Enabled:=s4.Checked;
  OnyOne.Checked:=SystemSets.OnyOne;
  except
  end;
end;

procedure Tsetting.FormShow(Sender: TObject);
begin                    
  bsskinbutton3.SetFocus;
end;

procedure Tsetting.fylxChange(Sender: TObject);
begin
  if fylx.ItemIndex=2 then
    sjtitle.Caption:='时间间隔(毫秒)：'
  else sjtitle.Caption:='时间间隔(秒)：';
  if fylx.ItemIndex=0 then
    fysj.Value:=3
  else if fylx.ItemIndex=1 then
    fysj.Value:=30
  else fysj.Value:=300;
end;

procedure Tsetting.s4Click(Sender: TObject);
begin
  fylx.Enabled:=s4.Checked;
  fysj.Enabled:=s4.Checked;
end;

procedure Tsetting.VoiceSetClick(Sender: TObject);
var
  MyVoiceSets:TVoiceSets;
begin
  Reader.O1.Click;
  MyVoiceSets:=TVoiceSets.Create(self);
  MyVoiceSets.VoiceList.Items.Clear;
  MyVoiceSets.VoiceList.Items.AddStrings(Reader.Read.VoiceLists);
  MyVoiceSets.VoiceList.ItemIndex:=SystemSets.VoiceIndex;
  MyVoiceSets.rateset.Value:=SystemSets.VoiceRate;
  MyVoiceSets.volset.Value:=SystemSets.VoiceVol;
  MyVoiceSets.ShowModal;
  MyVoiceSets.Free;
end;

end.
