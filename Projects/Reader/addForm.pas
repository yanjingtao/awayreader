unit addForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, BusinessSkinForm, bsSkinCtrls, Mask,
  bsSkinBoxCtrls;

type
  TaddBook = class(TForm)
    Label1: TLabel;
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    filetype: TbsSkinComboBox;
    al: TbsSkinEdit;
    Filename: TbsSkinEdit;
    bsSkinStdLabel1: TbsSkinStdLabel;
    bsSkinStdLabel2: TbsSkinStdLabel;
    booknum: TbsSkinStdLabel;
    bsSkinStdLabel3: TbsSkinStdLabel;
    Button2: TbsSkinButton;
    Button3: TbsSkinButton;
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    //procedure FonttoString(Font: TFont; var S: string);
  public
    { Public declarations }
    id:string;
  end;

var
  addBook: TaddBook;

implementation
uses MainForm, FileManagerForm;

{$R *.dfm}

procedure TaddBook.Button2Click(Sender: TObject);
var
  fonts:string;
  id2,i:integer;
begin
  if not Fileexists(filename.Text) then
    exit;
  if al.Text='' then
    al.Text:=' ';
  if strtoint(id)>-1 then
  begin
    Reader.Query1.Close;
    Reader.Query1.SQL.Text:='update fileinfo SET 作者="'+al.Text+'",大小="'+Booknum.Caption+'",小说类型="'+inttostr(filetype.ItemIndex)+'" where id='+id;
    Reader.Query1.Open;
  end else
  with Reader do
  try
      Query1.Close;
      Query1.SQL.Clear;
      Fonttostring(Font,fonts);
      Query1.SQL.Add('INSERT INTO fileinfo (文件名,背景颜色,背景类型,背景模式,背景图片,字体,作者,大小,小说类型) VALUES("'+Filename.Text+'","'+inttostr(Color)+'","0","0"," ","'+fonts+'","'+al.Text+'","'+BookNum.Caption+'","'+inttostr(FileType.ItemIndex)+'")');
      Query1.Open;
      Query1.Close;
      Query1.SQL.Clear;
      Query1.SQL.Add('select * from fileinfo where 文件名="'+Filename.Text+'"');
      Query1.Open;
      id2:=Query1.FieldByName('id').Value;
      for i := 0 to 5 do
        try
          Query1.SQL.Clear;
          Query1.SQL.Add('INSERT INTO markinfo (id,position,number) VALUES("'+inttostr(id2)+'","-1","'+inttostr(i)+'")');
          Query1.Open;
        except
        end;
  except
  end;
  FileManager.LoadList;
  close;
end;

procedure TaddBook.Button3Click(Sender: TObject);
begin
  close;
end;

{procedure TaddBook.FonttoString(Font: TFont; var S: string);
var
  i:integer;
begin
  s:=Font.Name+'|'+inttostr(font.Size)+'|';
  i:=0;
  if fsBold in Font.Style then i:=1;
  if fsItalic in Font.Style then i:=i+(1 shl 1);
  if fsUnderline in Font.Style then i:=i+(1 shl 2);
  if fsStrikeOut in Font.Style then i:=i+(1 shl 3);
  s:=s+inttostr(i)+'|'+inttostr(font.Color);
end;
}
procedure TaddBook.FormCreate(Sender: TObject);
begin
  id:='-1';
end;

end.
