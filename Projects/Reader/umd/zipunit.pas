unit zipunit;

interface
uses Classes, zlib;

implementation

procedure PackStream(const src:TStream;Dst:TStream);
var
  CompStream:TCompressionStream;
begin
  Assert(Src<>Nil);
  Assert(Dst<>Nil);
  CompStream:=TCompressionStream.Create(clDefault,Dst);
  try
    src.Seek(0,soFromBeginning);
    CompStream.CopyFrom(src,0);
  finally
    CompStream.Free;
  end;
end;

procedure UnpackStream(const src:TStream;Dst:TStream);
var
  DecompStream:TDecompressionStream;
  NewSize:int64;
begin
  Assert(src<>Nil);
  Assert(dst<>Nil);
  DecompStream:=TDecompressionStream.Create(src);
  try
    NewSize:=src.Seek(0,soFromEnd);
    Src.Seek(0,soFromBeginning);
    NewSize:=Dst.CopyFrom(DecompStream,NewSize);
    Dst.Size:=NewSize;
  finally
    DecompStream.Free;
  end;
end;

end.
