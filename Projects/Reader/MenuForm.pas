unit MenuForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BusinessSkinForm, bsSkinCtrls, bsSkinBoxCtrls, StdCtrls, Mask, VBScript_RegExp_55_TLB,
  OleServer;

type
  TgetMenu = class(TForm)
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    bsSkinRadioGroup1: TbsSkinRadioGroup;
    search: TbsSkinButton;
    showMenu: TbsSkinListBox;
    bsSkinButton1: TbsSkinButton;
    bsSkinButton2: TbsSkinButton;
    clickandgo: TbsSkinCheckRadioBox;
    bsSkinButton3: TbsSkinButton;
    RegExp1: TRegExp;
    SearchText: TbsSkinComboBox;
    procedure bsSkinButton2Click(Sender: TObject);
    procedure bsSkinButton1Click(Sender: TObject);
    procedure searchClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SearchTextKeyPress(Sender: TObject; var Key: Char);
    procedure showMenuListBoxClick(Sender: TObject);
    procedure showMenuClick(Sender: TObject);
    procedure bsSkinButton3Click(Sender: TObject);
  private
    { Private declarations }
    Positions:TStringList;
  public
    { Public declarations }
  end;

var
  getMenu: TgetMenu;

implementation
uses MainForm,publicvar;

{$R *.dfm}

procedure TgetMenu.bsSkinButton1Click(Sender: TObject);
begin
  BookMarks.AddStrings(Positions);
  Reader.status.Caption:='正在添加目录信息到数据库...';
  Reader.UpdateMarkList;
  Reader.status.OnClick(Reader.status);
  Close;
end;

procedure TgetMenu.bsSkinButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TgetMenu.bsSkinButton3Click(Sender: TObject);
begin
  if showMenu.ItemIndex>=0 then
    begin
      Positions.Delete(showMenu.ItemIndex);
      showMenu.Items.Delete(showMenu.ItemIndex);
    end;
end;

procedure TgetMenu.FormCreate(Sender: TObject);
begin
  Positions:=TStringList.Create;
end;

procedure TgetMenu.searchClick(Sender: TObject);
var
  s1,s2:WideString;
  machs: IMatchCollection;
  Matchs: Match;
  submatch: ISubMatches;
  i: integer;
begin
  s1:=Reader.Read.Text;
  s2:=SearchText.Text;
  RegExp1.Global := true;
  RegExp1.Pattern:=s2;
  RegExp1.IgnoreCase := true;
  machs := RegExp1.Execute(s1) as IMatchCollection;
  Positions.Clear;
  showMenu.Items.Clear;
  for i := 0 to machs.Count - 1 do
  begin
    Matchs := machs.Item[i] as Match;
    submatch := Matchs.SubMatches as ISubMatches;
    showMenu.Items.Add(matchs.Value);
    Positions.Add(inttostr(matchs.FirstIndex));
  end;
end;

procedure TgetMenu.SearchTextKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
    search.OnClick(search);
end;

procedure TgetMenu.showMenuClick(Sender: TObject);
begin
  if showMenu.ItemIndex<0 then
    bsskinbutton3.Enabled:=False
  else bsskinbutton3.Enabled:=True;
end;

procedure TgetMenu.showMenuListBoxClick(Sender: TObject);
begin
  if clickandgo.Checked then Reader.Read.Pos:=strtoint(Positions[showMenu.ItemIndex]);
  if showMenu.ItemIndex<0 then
    bsskinbutton3.Enabled:=False
  else bsskinbutton3.Enabled:=True;
end;

end.
