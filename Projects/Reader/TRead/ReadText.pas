unit ReadText;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, ExtCtrls, Jpeg, GIFImg,
  SpeechLib_TLB, StdCtrls, DateUtils, Dialogs;

type
  TReadText = class(TPaintBox)
  private
    FActive:    Boolean;            //显示？
    FAutoCross: Boolean;            //自动翻页
    FCrossType: Integer;            //翻页类型   0:按行翻页 1:按页翻页 2:按像素翻页
    FCrossTime: Integer;            //翻页时间
    FCrossTimer:TTimer;             //翻页控件
    FText:      WideString;         //显示文本
    FPos:       Integer;            //当前位置
    FLength:    Integer;            //文本长度
    FLineLength:array of Integer;   //每行字数
    FPreLineLength:array of Integer;//前N行字数
    FCorrectY:  Integer;            //修正坐标
    FRows:      Integer;            //行距
    FCols:      Integer;            //字符间距
    FBgType:    Boolean;            //背景模式
    FBgFile:    String;             //背景图片
    FBgMode:    Integer;            //背景图片显示模式   0:居中 1:拉伸 2:平铺
    FVoiceLists:TStringList;        //语音列表
    FVoiceIndex:Integer;            //语音选择
    FVoiceRate: Integer;            //语速选择
    FVoiceVol:  Integer;            //音量大小
    FVoiceInfor:String;             //语音信息
    FVoice:     TSpVoice;           //语音控件
    FVoiceChanged:Boolean;          //更改了位置
    FVoiceStart:Boolean;            //播放语音
    FReadText:WideString;           //语音临时变量
    FGB:        Integer;            //简繁转换 0：不转换 1：简体转繁体 2：繁体转简体
    Pic:TPicture;                   //背景图片载入容器
    Ca:TBitmap;                     //显示缓冲区
    FComplete  :Boolean;            //标记结束
    FOnChanged:TNotifyEvent;
    FOnPaint: TNotifyEvent;
    FOnReadComplete: TNotifyEvent;
    procedure SetActive(Value: boolean);
    procedure SetText(Value: WideString);
    procedure SetBgType(Value: Boolean);
    procedure SetPos(Value:Integer);
    procedure SetCross(Value:Boolean);
    procedure SetVoiceIndex(Value:Integer);
    procedure SetVoiceVol(Value:Integer);
    procedure SetVoice(Value:TSpVoice);
    procedure CalcLinePos;
    procedure OnCrossTimerTimer(Sender: TObject);
    procedure OnStop(ASender: TObject; StreamNumber: Integer;
  StreamPosition: OleVariant);
    procedure OnRead(ASender: TObject; StreamNumber: Integer;
  StreamPosition: OleVariant; AudioLevel: Integer);
    procedure PaintVer;
    procedure speaktext;
  protected
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure nextLine(num:integer);
    procedure preLine(num:integer);
    procedure nextPage;
    procedure prePage;
    procedure nextParagraph;
    procedure Speak;
    procedure Stop;
    { Public declarations }
  published
    property Active :boolean read FActive write SetActive default false;
    property Align;
    property Anchors;
    property AutoCross:boolean read FAutoCross write SetCross default false;
    property BgFile:String read FBgFile write FBgFile;
    property BgMode:Integer read FBgMode write FBgMode;
    property BgType:Boolean read FBgType write SetBgType default false;
    property Color;
    property Cols:Integer read FCols write FCols;
    property Constraints;
    property CrossTime:Integer read FCrossTime write FCrossTime;
    property CrossType:Integer read FCrossType write FCrossType;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property GB:Integer read FGB write FGB;
    property Length:Integer read FLength;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property Pos:Integer read FPos write SetPos;
    property Rows:Integer read FRows write FRows;
    property ShowHint;
    property Text:WideString read FText write SetText;
    property Visible;
    property Voice:TSpVoice read FVoice write setVoice;
    property VoiceIndex:Integer read FVoiceIndex write setVoiceIndex;
    property VoiceInformation:String read FVoiceInfor;
    property VoiceLists:TStringList read FVoiceLists;
    property VoiceRate:Integer read FVoiceRate write FVoiceRate;
    property VoiceVol:Integer read FVoiceVol write setVoiceVol;
    property OnChanged: TNotifyEvent read FOnChanged write FOnChanged;
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnMouseActivate;
    property OnMouseDown;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnMouseMove;
    property OnMouseUp;
    property OnPaint: TNotifyEvent read FOnPaint write FOnPaint;
    property OnReadComplete: TNotifyEvent read FOnReadComplete write FOnReadComplete;
    property OnStartDock;
    property OnStartDrag;
    { Published declarations }
  end;

const Reader_version = ' V3.3.0 Final';

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Awaysoft', [TReadText]);
end;

{ TReadText }

function GBToBIG5(GBStr : String): WideString;
var
  Len: Integer;
  pGBCHTChar: PAnsiChar;
  pGBCHSChar: PAnsiChar;
  pUniCodeChar: PWideChar;
begin
  pGBCHSChar := PAnsiChar(AnsiString(GBStr));
  Len := MultiByteToWideChar(936,0,pGBCHSChar,-1,nil,0);
  GetMem(pGBCHTChar,Len*2+1);
  ZeroMemory(pGBCHTChar,Len*2+1);
  //GB CHS -> GB CHT
  LCMapStringA($804,LCMAP_TRADITIONAL_CHINESE,pGBCHSChar,-1,pGBCHTChar,Len*2);
  GetMem(pUniCodeChar,Len*2);
  ZeroMemory(pUniCodeChar,Len*2);
  //GB CHT -> UniCode
  MultiByteToWideChar(936,0,pGBCHTChar,-1,pUniCodeChar,Len*2);
  WideCharToMultiByte(950,0,pUniCodeChar,-1,nil,0,nil,nil);
  Result := WideString(pUniCodeChar);
  FreeMem(pGBCHTChar);
  FreeMem(pUniCodeChar);
end;

 

 

function BIG5ToGB(BIG5Str : WideString): WideString;
var
 Len: Integer;
 pGBCHSChar: PAnsiChar;
 pGBCHTChar: PAnsiChar;
 pUniCodeChar: PWideChar;
begin
  pUniCodeChar := PWideChar(BIG5Str);
  Len := WideCharToMultiByte(950,0,pUniCodeChar,-1,nil,0,nil,nil);
  GetMem(pGBCHTChar,Len*2);
  GetMem(pGBCHSChar,Len*2);
  ZeroMemory(pGBCHTChar,Len*2);
  ZeroMemory(pGBCHSChar,Len*2);
  //UniCode->GB CHT
  WideCharToMultiByte(936,0,pUniCodeChar,-1,pGBCHTChar,Len,nil,nil);
  //GB CHT -> GB CHS
  LCMapStringA($804,LCMAP_SIMPLIFIED_CHINESE,pGBCHTChar,-1,pGBCHSChar,Len);
  Result := WideString(AnsiString(pGBCHSChar));
  FreeMem(pGBCHTChar);
  FreeMem(pGBCHSChar);
end;

procedure TReadText.CalcLinePos;
var
  maxLine,x,y,pos,i:integer;
  W1,W2:integer;
  Wid,Wid2:integer;
begin
  pos:=FPos;
  maxLine:=(Height-Margins.Top-Margins.Bottom) div (Ca.Canvas.TextHeight('啊')+FRows)+2;//最多行数
  W1:=Ca.Canvas.TextWidth('a');W2:=Ca.Canvas.TextWidth('啊');//英文字符或者中文字符宽度
  setLength(FLineLength,maxLine);x:=1;y:=0;
  FLineLength[0]:=pos;
  try
  while(pos<=FLength)do
  begin
    if ord(FText[pos])<128 then
      Wid:=W1 else Wid:=W2;
    x:=x+Wid;
    if pos<>Flength then
      begin
        if ord(FText[pos+1])<128 then
          Wid2:=W1 else Wid2:=W2;
      end
    else Wid2:=Wid;
    if (ord(FText[pos])<32)and(ord(FText[pos])<>13) then
      FText[pos]:=WideChar(32);
    if (ord(FText[pos])=13) or (x>Width-Wid2-Margins.Left-Margins.Right) then
      begin
        x:=1;y:=y+1;
        FLineLength[y]:=pos+1;
      end;
    if y>=maxLine-1 then
      break;
    pos:=pos+1;
  end;
  if pos>FLength then
    for i := y+1 to maxLine - 1 do
      FLineLength[i]:=FLength+1;
  except
    MessageBox(parent.Handle,pchar('统计数目出错！'),pchar('错误'),mb_ok+MB_ICONERROR);
  end;
end;

constructor TReadText.Create(AOwner: TComponent);
begin
  inherited;
  Pic:=TPicture.Create;
  FCrossTimer:=TTimer.Create(self);
  FCrossTimer.OnTimer:=OnCrossTimerTimer;
  FVoiceLists:=TStringList.Create;
  FVoice:=nil;
  Ca:=TBitmap.Create;
  FPos         := 1;
  FLength      := 0;
  FCorrectY    := 0;
  FRows        := 0;
  FCols        := 0;
  Font.Color   := clBlack;
  Font.Name    := '宋体';
  Font.Size    := 12;
  FBgType      := False;
  Color        := $00FEF4E7;
  FText        :=' ';
  FVoiceStart  :=False;
  FGB          :=0;
end;

destructor TReadText.Destroy;
begin

  inherited;
end;

procedure TReadText.nextLine(num: integer);
begin
  FCorrectY:=0;
  if num>=System.Length(FLineLength) then
    num:=System.Length(FLineLength)-1;
  if num>=0 then setPos(FLineLength[num]);
end;

procedure TReadText.nextPage;
begin
  FCorrectY:=0;
  if System.Length(FLineLength)>0 then
    setPos(FLineLength[System.Length(FLineLength)-2]);
end;

procedure TReadText.nextParagraph;
var
  i:integer;
begin
  i:=1;
  if pos=length then
  begin
    exit;
  end;
  while((pos+i<=length) and (ord(Text[pos+i])<>13)) do
  begin
    i:=i+1;
  end;
  FCorrectY:=0;
  setPos(pos+i+1);
end;

procedure TReadText.OnCrossTimerTimer(Sender: TObject);
begin
  if FAutoCross=false then FCrossTimer.Enabled:=false else
  begin
    if FCrossType=2 then   //按像素翻页
      begin
        FCrossTimer.Interval:=FCrossTime;
        FCorrectY:=FCorrectY+1;
        if FCorrectY>=Ca.Canvas.TextHeight('啊') then
          begin
            nextLine(1);
          end
          else paint;
      end
    else if FCrossType=0 then     //按行翻页
      begin
        FCrossTimer.Interval:=FCrossTime*1000;
        nextLine(1);
      end
    else if FCrossType=1 then   //按页翻页
      begin
        FCrossTimer.Interval:=FCrossTime*1000;
        nextPage;
      end;
  end;
end;

procedure TReadText.OnRead(ASender: TObject; StreamNumber: Integer;
  StreamPosition: OleVariant; AudioLevel: Integer);
begin

end;

procedure TReadText.OnStop(ASender: TObject; StreamNumber: Integer;
  StreamPosition: OleVariant);
begin
  if FVoiceChanged then Pos:=Pos+System.Length(FReadText)+1;
  SpeakText;
end;

procedure TReadText.Paint;
var
  MyRect: TRect;
  i,j:integer;
  s:WideString;
  Flag:Boolean;
begin
  //inherited;
  Flag:=False;
  ca.Width:=Width;
  ca.Height:=Height;
  ca.Canvas.Font:=Font;
  SetBkMode(ca.Canvas.Handle,TRANSPARENT);//设置字体透明，也可以用ca.Canvas.Brush.Style:=bsClear;
  ca.Canvas.Pen.Color:=Color;
  ca.Canvas.Brush.Color:=Color;
  MyRect.Left:=0;
  MyRect.Top:=0;
  MyRect.Right:=width;
  MyRect.Bottom:=height;
  if BgType then
    if FileExists(FBgFile) then
    begin
      if FBgMode=0 then
        begin
          ca.Canvas.rectangle(0,0,Width,Height);
          Ca.Canvas.Draw((Width-Pic.Width)div 2,(Height-Pic.Height)div 2,Pic.Graphic);
        end else
      if FBgMode=1 then
        Ca.Canvas.StretchDraw(MyRect,Pic.Graphic) else
      begin
        for i := 0 to (Width div pic.Width) do
          for j := 0 to (Height div pic.Height) do
            Ca.Canvas.Draw(i*pic.Width,j*pic.Height,pic.Graphic);
      end;
    end else BgType:=false
  else ca.Canvas.rectangle(0,0,Width,Height);
  if FActive then begin
    SetTextCharacterExtra(Ca.Canvas.Handle, FCols);
    CalcLinePos;
    j:=Ca.Canvas.TextHeight('啊');
    for i := 0 to System.Length(FLineLength) - 2 do
      begin
        if i<>0 then
          if FLineLength[i]=FLineLength[i-1] then
            break;
        s:=copy(FText,FLineLength[i],FLineLength[i+1]-FLineLength[i]);
        if (FGB<0) or (FGB>2) then       //设置简繁转换
          FGB:=0;
        if FGB=1 then
          s:=GBToBig5(s)
        else if FGB=2 then
          s:=Big5ToGB(s);
        if system.length(s)>1 then
        begin
          if ord(s[1])<=32 then
            s:=copy(s,2,system.Length(s)-1);
          if ord(s[system.Length(s)])<=32 then
            s:=copy(s,1,system.Length(s)-1);
        end else if (Pos=Length)and(i=0) then
        begin
          Flag:=True;
          s:=WideString('--全书完--');
          SetCross(False);
          Ca.Canvas.TextOut(Margins.Left,i*(j+FRows)+1-FCorrectY+Margins.Top,s);
          if (Assigned(FOnReadComplete)) and (FComplete=False) then
          begin
            FComplete:=True;
            FOnReadComplete(Self);
          end;
        end;
        if not Flag then Ca.Canvas.TextOut(Margins.Left,i*(j+FRows)+1-FCorrectY+Margins.Top,s);
      end;
    //BitBlt(Canvas.Handle,0,0,Width,Height,Ca.Canvas.Handle,0,0,SRCCOPY);
    Canvas.CopyRect(MyRect,Ca.Canvas,MyRect);
    if Assigned(FOnPaint) then FOnPaint(Self);
  end else begin
    Canvas.CopyRect(MyRect,Ca.Canvas,MyRect);
    Paintver;
  end;
end;

procedure TReadText.PaintVer;
var
  len:integer;
begin
  SetBkMode(Canvas.Handle,TRANSPARENT);
  Canvas.Font.Size:=12;
  Canvas.Font.Name:='宋体';
  Canvas.Font.Color:=clBlack;
  Canvas.Font.Style:=[];
  len:=Canvas.TextWidth(Reader_version);
  Canvas.TextOut(Width-len-10,Height-Canvas.TextHeight('啊')*2-5,Reader_version);
  Canvas.Font.Size:=9;
  Canvas.Font.Name:='宋体';
  Canvas.Font.Color:=clBlack;
  len:=Canvas.TextWidth('www.awaysoft.com');
  Canvas.TextOut(Width-len-5,Height-Canvas.TextHeight('啊')-5,'www.awaysoft.com');
end;

procedure TReadText.preLine(num: integer);
var
  x,y,i,len1,lenset2,lenset1,line:integer;
begin
  if Ca.Height=0 then
    paint;
  x:=1;y:=1+FCorrectY;line:=0;setlength(FPreLineLength,0);
  if (FText='') or (FPos=0) then
    exit;
  for i := FPos-1 downto 0 do
    begin
      if i<0 then break;
      lenset1:=Ca.Canvas.TextWidth('a');lenset2:=Ca.Canvas.TextWidth('啊');
      if ord(FText[i])<128 then
        len1:=lenset1 else len1:=lenset2;
      x:=x+len1;
      if (self.Width-1-x<len1) or (ord(FText[i])=13)and (i<>FPos-1) or (i=0) then
        begin
          x:=1;
          setlength(FPreLineLength,line+1);
          if (ord(FText[i])=13) then FPreLineLength[line]:=i+1
            else FPreLineLength[line]:=i;
          if line>=num-1 then
            begin
              FCorrectY:=0;
              setPos(FPreLineLength[line]);
              exit;
            end;
          line:=line+1;
          y:=y+Ca.Canvas.TextHeight('啊')+FRows;
        end;
      if self.Height<y then
        break;
    end;
  FCorrectY:=0;
  setPos(1);
end;

procedure TReadText.prePage;
var
  x,y,i,len1,lenset2,lenset1,line:integer;
begin
  if Ca.Height=0 then
    paint;
  x:=1;y:=1+FCorrectY;line:=0;setlength(FPreLineLength,0);
  if (FText='') or (FPos=0) then
    exit;
  for i := FPos-1 downto 0 do
    begin
      if i<0 then break;
      lenset1:=Ca.Canvas.TextWidth('a');lenset2:=Ca.Canvas.TextWidth('啊');
      if ord(FText[i])<128 then
        len1:=lenset1 else len1:=lenset2;
      x:=x+len1;
      if (self.Width-1-x<len1) or (ord(FText[i])=13)and(i<>FPos-1) or (i=0) then
        begin
          x:=1;
          setlength(FPreLineLength,line+1);
          if ord(FText[i])=13 then FPreLineLength[line]:=i+1
            else FPreLineLength[line]:=i;
          line:=line+1;
          y:=y+Ca.Canvas.TextHeight('啊')+FRows;
        end;
      if self.Height<y then
        break;
    end;
  FCorrectY:=0;
  if line=1 then setPos(1)
  else if line>1 then setPos(FPreLineLength[line-2])
       
end;

procedure TReadText.SetActive(Value: boolean);
begin
  FActive:=Value;
  Paint;
end;

procedure TReadText.SetBgType(Value: Boolean);
begin
  FBgType:=Value;
  if FBgType then
     if FileExists(FBgFile) then
     try
       pic.LoadFromFile(FBgFile);
     except
       MessageBox(parent.Handle,pchar('背景图片打开失败，请重新设置背景图片！'),pchar('错误'),mb_ok+MB_ICONERROR);
       BgType:=false;
     end
     else begin
       // MessageBox(parent.Handle,pchar('背景图片不存在，请重新设置背景图片！'),pchar('错误'),mb_ok+MB_ICONERROR);
       BgType:=false;
     end;
  Paint;
end;

procedure TReadText.SetCross(Value: Boolean);
begin
  FAutoCross:=Value;
  if FCrossType=2 then FCrossTimer.Interval:=FCrossTime
  else FCrossTimer.Interval:=FCrossTime*1000;
  FCrossTimer.Enabled:=Value;
end;

procedure TReadText.SetPos(Value: Integer);
begin
  if Value<1 then
    Value:=1;
  if Value>FLength then
    Value:=FLength;
  FPos:=Value;
  FVoiceChanged:=False;
  FComplete:=False;
  Paint;
  if Assigned(FOnChanged) then FOnChanged(Self);
end;

procedure TReadText.SetText(Value: WideString);
begin
  if Value='' then
    Value:=' ';
  FText:=Value;
  FLength:=System.Length(FText);
  if FPos>FLength then
    FPos:=1;
  FGB:=0;
  if Assigned(FOnChanged) then FOnChanged(Self);
end;

procedure TReadText.SetVoice(Value: TSpVoice);
var
  I: Integer;
  SOToken: ISpeechObjectToken;
  SOTokens: ISpeechObjectTokens;
begin
  FVoice:=Value;
  if FVoice<>nil then
  begin
    FVoice.EventInterests := SVEAllEvents;
    SOTokens := FVoice.GetVoices('', '');
    FVoiceLists.Clear;
    for I := 0 to SOTokens.Count - 1 do
    begin
      SOToken := SOTokens.Item(I);
      FVoiceLists.AddObject(SOToken.GetDescription(0), TObject(SOToken));
      SOToken._AddRef;
    end;
    if FVoiceLists.Count>0 then
      setVoiceIndex(0);
    FVoiceRate:=FVoice.Rate;
    FVoiceVol:=FVoice.Volume;
    FVoice.OnEndStream:=OnStop;
    FVoice.OnAudioLevel:=OnRead;
  end;
end;

procedure TReadText.SetVoiceIndex(Value: Integer);
var
  Voices: ISpeechObjectTokens;
  SOToken: ISpeechObjectToken;
  ws: WideString;
begin
  if FVoiceLists.Count=0 then
    exit;
  if Value>=FVoiceLists.Count then
    Value:=FVoiceLists.Count-1;
  FVoiceIndex:=Value;
  Voices := FVoice.GetVoices('','');
  SOToken := Voices.Item(Value);
  FVoice.Voice := SOToken;

  FVoiceInfor:=Format('名称: %s', [SOToken.GetAttribute('Name')])+#10#13;
  FVoiceInfor:=FVoiceInfor+Format('厂商: %s', [SOToken.GetAttribute('Vendor')])+#10#13;
  FVoiceInfor:=FVoiceInfor+Format('性别: %s', [SOToken.GetAttribute('Gender')])+#10#13;
  FVoiceInfor:=FVoiceInfor+Format('语言: %s', [SOToken.GetAttribute('Language')])+#10#13;
  FVoiceInfor:=FVoiceInfor+#10#13+'语言说明:804=中文 409=英语';
end;

procedure TReadText.SetVoiceVol(Value: Integer);
begin
  if Value>100 then Value:=100;
  if Value<0 then Value:=0;
  FVoiceVol:=Value;
  if FVoice<>nil then FVoice.Volume:=Value;
end;

procedure TReadText.Speak;
begin
  if FVoice<>nil then
    begin
      FVoice.Connect;
      SetVoiceIndex(FVoiceIndex);
      FVoice.Volume:=FVoiceVol;
      FVoice.Rate:=FVoiceRate;
      FVoiceStart:=True;
      SpeakText;
    end;
end;

procedure TReadText.SpeakText;
var
  i:integer;
begin
  if not FVoiceStart then
    exit;
  i:=1;
  FVoiceChanged:=True;
  if pos=length then
  begin
    Stop;
    exit;
  end;
  while((pos+i<=length) and (ord(Text[pos+i])<>13)) do
  begin
    i:=i+1;
  end;
  FReadText:=(copy(Text,pos,i));
  FVoice.Speak(FReadText,SVSFlagsAsync);
end;

procedure TReadText.Stop;
begin
  //if FVoice<>nil then FVoice.Skip('Sentence', MaxInt);
  FVoice.Disconnect;
  FVoiceStart:=False;
end;

end.
