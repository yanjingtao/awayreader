; 脚本由 Inno Setup 脚本向导 生成！
; 有关创建 Inno Setup 脚本文件的详细资料请查阅帮助文档！

#define MyAppName2 "AwayReader 精简版"
#define MyAppName "AwayReader"
#define MyAppVerName "AwayReader 3.3.0 Final"
#define MyAppPublisher "Awaysoft.Com"
#define MyAppURL "http://www.awaysoft.com/"
#define MyAppExeName "AwayReader.exe"

[Setup]
; 注: AppId的值为单独标识该应用程序。
; 不要为其他安装程序使用相同的AppId值。
; (生成新的GUID，点击 工具|在IDE中生成GUID。)
AppId={{94FBC54F-B42D-47B3-814D-AAF6250790F0}
AppName={#MyAppName2}
AppVerName={#MyAppVerName}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\Awaysoft\AwayReader
DefaultGroupName=Awaysoft\AwayReader
AllowNoIcons=yes
LicenseFile=D:\Projects\Reader\版权信息.rtf
InfoAfterFile=D:\Projects\Reader\版本信息.rtf
OutputDir=D:\Projects\Reader\精简版输出目录
OutputBaseFilename=AwayReader
SetupIconFile=D:\Projects\Reader\ico.ico
Compression=lzma
SolidCompression=yes
VersionInfoVersion=3.3.0.57
VersionInfoCompany=Awaysoft.Com
VersionInfoDescription=AwayReader精简版 安装程序
VersionInfoTextVersion=3, 3, 0, 57
VersionInfoCopyright=版权所有 (C) 2008-2012 严晶涛

[Languages]
Name: "chinesesimp"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkedonce
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkedonce

[Files]
Source: "D:\Projects\Reader\AwayReader.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Projects\Reader\背景.jpg"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Projects\Reader\版本信息.rtf"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Projects\Reader\版权信息.rtf"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Projects\Reader\sqlite3.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Projects\Reader\plugins.ini"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\Projects\Reader\plugins\*"; DestDir: "{app}\plugins"; Flags: ignoreversion recursesubdirs createallsubdirs
; 注意: 不要在任何共享系统文件上使用“Flags: ignoreversion”

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Comment: "简单好用的电子书阅读器"; HotKey: "ctrl+alt+r"
Name: "{group}\参数说明"; Filename: "{app}\{#MyAppExeName}"; Parameters:"-h"; Comment: "查看电子书阅读器的参数"
Name: "{group}\{cm:ProgramOnTheWeb,{#MyAppPublisher}}"; Filename: "{#MyAppURL}"; Comment: "Awaysoft.Com";IconFilename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"; Comment: "删除AwayReader"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Comment: "简单好用的电子书阅读器"; Tasks: desktopicon
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Comment: "简单好用的电子书阅读器"; Tasks: quicklaunchicon

[Registry]
Root: HKCU; Subkey: "Software\Awaysoft"; Flags: uninsdeletekeyifempty;
Root: HKCU; Subkey: "Software\Awaysoft\AwayReader"; Flags: uninsdeletekey;
Root: HKCR; Subkey: "txtfile\shell\使用AwayReader打开(&T)"; Flags: uninsdeletekey;
Root: HKCR; Subkey: "txtfile\shell\使用AwayReader打开(&T)\command"; Flags: uninsdeletekey;
Root: HKCR; Subkey: "txtfile\shell\使用AwayReader打开(&T)\command"; ValueType: string; ValueName: ""; ValueData: """{app}\{#MyAppExeName}"" ""%1"""; Flags: uninsdeletekey;

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#MyAppName}}"; Flags: nowait postinstall skipifsilent

[Code]
procedure AboutButtonOnClick(Sender: TObject);
begin
  MsgBox('Awaysoft.Com出品', mbInformation, mb_Ok);
end;

procedure URLLabelOnClick(Sender: TObject);
var
  ErrorCode: Integer;
begin
  ShellExec('open', 'http://www.awaysoft.com', '', '', SW_SHOWNORMAL, ewNoWait, ErrorCode);
end;

procedure CreateAboutButtonAndURLLabel(ParentForm: TSetupForm; CancelButton: TNewButton);
var
  AboutButton: TNewButton;
  URLLabel: TNewStaticText;
begin
  AboutButton := TNewButton.Create(ParentForm);
  AboutButton.Left := ParentForm.ClientWidth - CancelButton.Left - CancelButton.Width;
  AboutButton.Top := CancelButton.Top;
  AboutButton.Width := CancelButton.Width;
  AboutButton.Height := CancelButton.Height;
  AboutButton.Caption := '关于(&A)';
  AboutButton.OnClick := @AboutButtonOnClick;
  AboutButton.Parent := ParentForm;

  URLLabel := TNewStaticText.Create(ParentForm);
  URLLabel.Caption := 'www.awaysoft.com';
  URLLabel.Cursor := crHand;
  URLLabel.OnClick := @URLLabelOnClick;
  URLLabel.Parent := ParentForm;
  { Alter Font *after* setting Parent so the correct defaults are inherited first }
  URLLabel.Font.Style := URLLabel.Font.Style + [fsUnderline];
  URLLabel.Font.Color := clBlue;
  URLLabel.Top := AboutButton.Top + AboutButton.Height - URLLabel.Height - 2;
  URLLabel.Left := AboutButton.Left + AboutButton.Width + ScaleX(20);
end;

function InitializeSetup(): Boolean;
begin
  Result:=True;
end;

procedure InitializeWizard();
begin
  CreateAboutButtonAndURLLabel(WizardForm, WizardForm.CancelButton);
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
var
  s: AnsiString;
begin
  case CurUninstallStep of
    usUninstall:
      begin
        // if MsgBox('是否删除书签文件和配置文件(不可恢复)？', mbConfirmation, MB_YESNO)=MRYES then
        //   begin
        //     s:= ExpandConstant('{app}\data.dat');
        //     DeleteFile(s);
        //  end;
      end;
    usPostUninstall:
      begin
        //
      end;
  end;
end;

procedure DeinitializeUninstall();
var
  ErrorCode:integer;
begin
  if MsgBox('访问 Awaysoft 网站？', mbConfirmation, MB_YESNO)=MRYES then
    ShellExec('open', 'http://www.awaysoft.com', '', '', SW_SHOWNORMAL, ewNoWait, ErrorCode);
end;
