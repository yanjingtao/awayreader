library Project1;

uses
  SysUtils,
  Windows, 
  VCLUnZip, 
  Classes;
{$R *.res}


function GetTempDir: String;//获取系统临时目录
var
  Buffer: array[0..MAX_PATH] of Char;
begin
  GetTempPath(SizeOf(Buffer) - 1, Buffer);
  Result := StrPas(Buffer);
end;

function GetTempName: String;//生成一个临时文件名
var
  p,f: array[0..MAX_PATH] of Char;
begin
  GetTempPath(MAX_PATH, p);
  randomize;
  GetTempFileName(p,'~jar',random(10000),f);
  result:=f;
end;


function PluginType(Ver:integer): PChar;      //插件类型
begin
  result:=pchar('FileOpen');
end;

function UrlPosition(Ver:integer): Pchar;     //文件名位置
begin
  result:=pchar('end');
end;

function UrlKeyWord(Ver:integer): Pchar;      //后缀或前缀
begin
  result:=pchar('.jar');
end;

function ReadExtFile(Filename:Pchar):Pchar;     //读取开始
var
  unzip: TVCLUnZip;
  i,j:integer;
  TS:TStringList;
  WS:WideString;
  TempWide:WideString;
  TempString:String;
  FS:TMemoryStream;
  MyFilename:string;
  types:integer;
begin
  MyFilename:=Filename;
  try
  types:=1;
  unzip:=TVCLUnzip.Create(nil);
  unzip.ZipName:=MyFilename;
  unzip.OverwriteMode:=Always;
  UnZip.ReadZip;
  TS:=TStringList.Create;
  for I := 0 to unzip.Count - 1 do
    if (pos('.',unzip.filename[i])=0)and(unzip.Filename[i]<>'') then
    begin
      TS.Add(unzip.Filename[i]);
      UnZip.FilesList.Add(UnZip.Filename[i] );
    end;
  if TS.Count=0 then
  begin
    types:=2;
    for I := 0 to unzip.Count - 1 do
    if (pos('.r',unzip.filename[i])<>0) then
    begin
      TS.Add(unzip.Filename[i]);
      UnZip.FilesList.Add(UnZip.Filename[i] );
    end;
    j:=Ts.Count;
    TS.Clear;
    for i := 0 to j - 1 do
      TS.Add(inttostr(i)+'.r');
  end else
  begin
    j:=TS.Count;
    TS.Clear;
    for i := 1 to j-1 do
      TS.Add(inttostr(i));
  end;
  UnZip.DoAll:=false;
  UnZip.DestDir:=GetTempDir;//指定目录
  UnZip.RecreateDirs := True;
  UnZip.RetainAttributes := True;
  UnZip.UnZip;
  DeleteFile(pchar(GetTempDir+'0'));
  WS:='';
  if Types=1 then
  for I := 0 to TS.Count - 1 do
    begin
      FS:=TMemoryStream.Create;
      FS.LoadFromFile(GetTempDir+TS[i]);
      DeleteFile(pchar(GetTempDir+TS[i]));
      SetLength(TempWide,(Fs.Size) div 2);
      Fs.Read(TempWide[1],Fs.Size);
      WS:=WS+TempWide;
      FS.Free;
    end
  else if Types=2 then
  for I := 0 to TS.Count - 1 do
    begin
      TempString:='';
      FS:=TMemoryStream.Create;
      FS.LoadFromFile(GetTempDir+TS[i]);
      DeleteFile(pchar(GetTempDir+TS[i]));
      FS.Seek(3,0);
      SetLength(TempString,FS.Size-3);
      FS.Read(TempString[1],FS.Size-3);
      TempWide:=Utf8Decode(TempString);
      WS:=WS+TempWide;
      FS.Free;
    end;
  TS.Text:=AnsiString(WS);
  MyFilename:=GetTempName;
  TS.SaveToFile(MyFilename);
  TS.Free;
  UnZip.Free;
  except
    UnZip.Free;
    Result:=PChar('');
    exit;
  end;
  Result:=PChar(MyFilename);
end;

exports
PluginType ,UrlPosition ,UrlKeyWord ,ReadExtFile;

begin
end.
