unit CopyForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BusinessSkinForm, bsSkinCtrls, StdCtrls, bsSkinBoxCtrls;

type
  TCopyText = class(TForm)
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    bsSkinScrollBar1: TbsSkinScrollBar;
    bsSkinPanel1: TbsSkinPanel;
    bsSkinButton1: TbsSkinButton;
    Cancel: TbsSkinButton;
    Memo: TbsSkinMemo2;
    procedure CancelClick(Sender: TObject);
    procedure bsSkinButton1Click(Sender: TObject);
    procedure MemoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure MemoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CopyText: TCopyText;

implementation
uses MainForm;

{$R *.dfm}

procedure TCopyText.bsSkinButton1Click(Sender: TObject);
begin
  Memo.CopyToClipboard;
  close;
end;

procedure TCopyText.CancelClick(Sender: TObject);
begin
  close;
end;

procedure TCopyText.MemoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Memo.SelLength=0 then
    bsskinbutton1.Enabled:=false
  else bsskinbutton1.Enabled:=true;
end;

procedure TCopyText.MemoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Memo.SelLength=0 then
    bsskinbutton1.Enabled:=false
  else bsskinbutton1.Enabled:=true;
end;

end.
