unit FileManagerForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BusinessSkinForm, bsSkinCtrls, bsSkinBoxCtrls, Menus, bsSkinMenus,
  DB, ASGSQLite3, bsSkinShellCtrls;

type
  TFileManager = class(TForm)
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    BookTypeList: TbsSkinComboBox;
    BookList: TbsSkinListBox;
    StatusBar: TbsSkinStatusBar;
    Panel1: TbsSkinPanel;
    BtnAddBook: TbsSkinButton;
    BtnModifyBook: TbsSkinButton;
    BtnDeleteBook: TbsSkinButton;
    AddMenu: TbsSkinPopupMenu;
    M1: TMenuItem;
    F1: TMenuItem;
    Query1: TASQLite3Query;
    SelectDirectoryDialog1: TbsSkinSelectDirectoryDialog;
    StatusPanel1: TbsSkinStatusPanel;
    BtnRefresh: TbsSkinButton;
    Query2: TASQLite3Query;
    procedure BtnAddBookClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BookListListBoxClick(Sender: TObject);
    procedure LoadList();
    procedure FormCreate(Sender: TObject);
    procedure BookTypeListChange(Sender: TObject);
    procedure F1Click(Sender: TObject);
    procedure BtnModifyBookClick(Sender: TObject);
    procedure BookListListBoxDblClick(Sender: TObject);
    procedure M1Click(Sender: TObject);
    procedure BtnDeleteBookClick(Sender: TObject);
    procedure BtnRefreshClick(Sender: TObject);
  private
    { Private declarations }
    SQL:string;
  public
    { Public declarations }
    function inttotype(i:integer):string;
    function checkList(STL:TStringList):TStringList;
    function getNewFiles(path:string):TStringList;  //获取某个目录添加的新文件
  end;

  fileinfo = record
    id:integer;
    filename:string;
    booktype:byte;
    size:integer;
    author:string;
  end;

var
  FileManager: TFileManager;
  filenum:integer;
  files:array of fileinfo;

implementation
uses MainForm, addForm, publicvar;

{$R *.dfm}

procedure TFileManager.BtnAddBookClick(Sender: TObject);
begin
  addMenu.Popup(mouse.CursorPos.X,mouse.CursorPos.Y);
end;

procedure TFileManager.BtnDeleteBookClick(Sender: TObject);
var
  id,index:Integer;
begin
  index:=BookList.ItemIndex-1;
  if index>=0 then
  begin
    id:=files[index].id;
    if Reader.RMessage.MessageDlg('是否删除这条记录？'+#13#10+files[index].filename,mtConfirmation,[mbYES,mbNO],0)=mrYES then
      begin
        Query1.SQL.Text:='delete from fileinfo where id="'+inttostr(id)+'"';
        Query1.ExecSQL;
        LoadList;
      end;
  end;
end;

procedure TFileManager.BtnModifyBookClick(Sender: TObject);
var
  addBookForm:TaddBook;
  index:Integer;
begin
  index:=BookList.ItemIndex-1;
  if index>=0 then
  begin
    addBookForm:=TaddBook.Create(self);
    addBookForm.id:=inttostr(files[index].id);
    addBookForm.Filename.Text:=files[index].filename;
    addBookForm.filetype.ItemIndex:=files[index].booktype;
    addBookForm.booknum.Caption:=inttostr(files[index].size);
    addBookForm.al.Text:=files[index].author;
    addBookForm.ShowModal;
    addBookForm.Free;
  end;
end;

procedure TFileManager.BtnRefreshClick(Sender: TObject);
begin
  LoadList;
end;

function TFileManager.checkList(STL: TStringList): TStringList;
var
  i:integer;
  sql:string;
begin
  if STL.Count=0 then
    result:=STL
  else begin
    sql:='select * from fileinfo ';
    for i := 0 to STL.Count - 1 do
      begin
        STL[i]:=LowerCase(STL[i]);
        if i=0 then
          sql:=sql+'where 文件名='''+STL[i]+''' '
        else sql:=sql+'or 文件名='''+STL[i]+''' ';
      end;
    Query1.SQL.Text:=sql;
    Query1.Open;
    while not Query1.Eof do
    begin
      for i := 0 to STL.Count - 1 do
        if Query1.FieldByName('文件名').Value=STL[i] then
        begin
          STL.Delete(i);
          break;
        end;
      Query1.Next;
    end;
    result:=STL;
  end;
end;

procedure TFileManager.BookListListBoxClick(Sender: TObject);
var
  index:integer;
  position:integer;
  percent:string;
begin
  index:=BookList.ItemIndex-1;
  if index<=-1 then
    BookList.Hint:=''
  else begin
    Query2.Close;
    Query2.SQL.Text:='select position from markinfo where id='''+inttostr(files[index].id)+''' and number=''0''';
    Query2.Open;
    if not Query2.IsEmpty then
    begin
      Query2.First;
      position:=Query2.FieldByName('position').Value
    end
    else position:=0;
    if files[index].size<=0 then percent:='阅读进度未知'
    else percent:=format('目前已阅读%d字  %.2f%%',[position,position/files[index].size*100]);
    BookList.Hint:='文件名：'+files[index].filename+#13#10+
                   '作者：'+files[index].author+#13#10+
                   '小说类型：'+inttotype(files[index].booktype)+#13#10+
                   '字数：'+inttostr(files[index].size)+#13#10+
                   percent
  end;
  if index>-1 then
  begin
    BtnModifyBook.Enabled:=True;
    BtnDeleteBook.Enabled:=True;
  end else
  begin
    BtnModifyBook.Enabled:=False;
    BtnDeleteBook.Enabled:=False;
  end;
end;

procedure TFileManager.BookListListBoxDblClick(Sender: TObject);
var
  index:integer;
begin
  index:=BookList.ItemIndex-1;
  if index>=0 then
  begin
    Reader.OpenDialog1.FileName:=files[index].filename;
    Reader.OpenFile(files[index].filename);
  end;
end;

procedure TFileManager.BookTypeListChange(Sender: TObject);
var
  index:integer;
begin
  index:=BookTypeList.ItemIndex;
  if index=0 then
    SQL:='select * from fileinfo'
  else if index<>8 then SQL:='select * from fileinfo where 小说类型='+inttostr(index)
  else SQL:='select * from fileinfo where 小说类型<1 or 小说类型>7';
  LoadList;
end;

procedure TFileManager.F1Click(Sender: TObject);
var
  addBookForm:TaddBook;
  id:integer;
begin
  if Reader.OpenDialog1.Execute then
    begin
      if Reader.OpenFile(Reader.OpenDialog1.FileName) then
      begin
        addBookForm:=TaddBook.Create(self);
        Query1.SQL.Text:='select id from fileinfo where 文件名='''+Reader.OpenDialog1.FileName+'''';
        Query1.Open;
        if not Query1.IsEmpty then
          id:=Query1.FieldByName('id').Value
        else id:=-1;
        addBookForm.id:=inttostr(id);
        addBookForm.Filename.Text:=Reader.OpenDialog1.FileName;
        addBookForm.booknum.Caption:=inttostr(Reader.Read.Length);
        addBookForm.ShowModal;
        addBookForm.Free;
      end;
    end else
    begin
      Reader.status.OnClick(Reader.status);
      Reader.OpenDialog1.FileName:=LastFilename;
    end;
end;

procedure TFileManager.FormCreate(Sender: TObject);
var
  FileList:TStringList;
  fonts:String;
  i:integer;
begin
  SQL:='select * from fileinfo';
  if DirectoryExists(SystemSets.DetectDir) then
  begin
    FileList:=getNewFiles(SystemSets.DetectDir);
    if FileList.Count>0 then
      if Reader.RMessage.MessageDlg('目录：'+SystemSets.DetectDir+' 新增了'+inttostr(FileList.Count)+'个文件'+#13#10+FileList.Text+#13#10+'是否添加这些文件？',mtConfirmation,[mbYES,mbNO],0)=mrYES then
        begin
          Query1.SQL.Text:='BEGIN;';
          Query1.ExecSQL;
          Reader.Fonttostring(Systemsets.Font,fonts);
          for i := 0 to FileList.Count - 1 do
          begin
            Query1.SQL.Text:='INSERT INTO fileinfo (文件名,背景颜色,背景类型,背景模式,背景图片,字体,作者,大小,小说类型) VALUES("'+FileList[i]+'","'+inttostr(SystemSets.Color)+'","'+booleantostr(SystemSets.BgType)+'","'+inttostr(SystemSets.BgMode)+'","'+SystemSets.BgFile+'","'+fonts+'"," ","'+inttostr(0)+'","0")';
            Query1.ExecSQL;
          end;
            Query1.SQL.Text:='COMMIT;';
            Query1.ExecSQL;
        end;
  end
end;

procedure TFileManager.FormShow(Sender: TObject);
begin
  Left:=Reader.Left-Width;
  if Left + Width < 50 then
    Left := 0;
  Top:=Reader.Top;
  if BookTypeList.ItemIndex=0 then LoadList;
end;

function TFileManager.getNewFiles(path: string): TStringList;
  function isneed(s:string):boolean;
  var
    i:integer;
    b:boolean;
  begin
    b:=false;
    if s='.txt' then
      b:=true
    else for i := 0 to Reader.TailExt.Count - 1 do
      if copy(Reader.TailExt[i],1,pos('=',Reader.TailExt[i])-1)=s then
        b:=true;
    result:=b;
  end;
var
  SRC:TSearchrec;
  FileList:TStringList;
begin
  FileList:=TStringList.Create;
  if FindFirst(path+'*',faAnyFile,SRC)=0 then
  repeat
    if (SRC.Name='.') or (SRC.Name='..') then continue;
    if DirectoryExists(path+SRC.Name) then continue;
    if isneed(ExtractFileExt(path+SRC.Name)) then
      FileList.Add(path+SRC.Name);
  until FindNext(SRC)<>0;
  FileList:=checkList(FileList);
  result:=FileList;
end;

function TFileManager.inttotype(i: integer): string;
const s:array[1..8] of string=('玄幻奇幻','武侠仙侠','都市言情','历史军事','游戏竞技','科幻灵异','耽美同人','其他类型');
begin
  if (i>=1) and (i<=8) then
    result:=s[i]
  else result:=s[8];
end;

procedure TFileManager.LoadList;
var
  i:integer;
begin
  Query1.SQL.Text:=SQL;
  Query1.Open;
  BookList.Clear;
  BookList.Items.Add(format('%-20s |%-15s',['文件名','作者']));
  filenum:=Query1.RecordCount;
  setlength(files,filenum);i:=0;
  while not Query1.Eof do
  begin
    files[i].id:=Query1.FieldByName('id').Value;
    files[i].filename:=Query1.FieldByName('文件名').Value;
    files[i].booktype:=Query1.FieldByName('小说类型').Value;
    files[i].size:=Query1.FieldByName('大小').Value;
    files[i].author:=Query1.FieldByName('作者').Value;
    BookList.Items.Add(format('%-20s |%-15s',[copy(ExtractFileName(files[i].filename),1,20),files[i].author]));
    Query1.Next;inc(i);
  end;
  BtnModifyBook.Enabled:=False;
  BtnDeleteBook.Enabled:=False;
  StatusPanel1.Caption:='这个分类共有文件：'+inttostr(filenum)+'个';
end;

procedure TFileManager.M1Click(Sender: TObject);
var
  path,fonts:string;
  i:integer;
  FileList:TStringList;
begin
  if SelectDirectoryDialog1.Execute then
    begin
      path:=SelectDirectoryDialog1.Directory;
      if path[length(path)]<>'\' then
        path:=path+'\';
      if DirectoryExists(path) then
      begin
        FileList:=getNewFiles(path);
        if FileList.Count>0 then
        begin
          Query1.SQL.Text:='BEGIN;';
          Query1.ExecSQL;
          Reader.Fonttostring(Systemsets.Font,fonts);
          for i := 0 to FileList.Count - 1 do
          begin
            Query1.SQL.Text:='INSERT INTO fileinfo (文件名,背景颜色,背景类型,背景模式,背景图片,字体,作者,大小,小说类型) VALUES("'+FileList[i]+'","'+inttostr(SystemSets.Color)+'","'+booleantostr(SystemSets.BgType)+'","'+inttostr(SystemSets.BgMode)+'","'+SystemSets.BgFile+'","'+fonts+'"," ","'+inttostr(0)+'","0")';
            Query1.ExecSQL;
          end;
            Query1.SQL.Text:='COMMIT;';
            Query1.ExecSQL;
        end;
        LoadList;
        if Reader.RMessage.MessageDlg('是否监视这个目录？'+#13#10+path,mtConfirmation,[mbYES,mbNO],0)=mrYES then
        begin
          SystemSets.DetectDir:=path;
        end;
      end else
        begin
          if Reader.RMessage.MessageDlg('文件夹不存在！'+#13#10+'　　是否取消监视目录功能？',mtConfirmation,[mbYES,mbNO],0)=mrYES then
          begin
            SystemSets.DetectDir:=' ';
          end;
        end;
    end;
end;

end.
