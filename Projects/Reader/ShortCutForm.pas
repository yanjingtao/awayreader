unit ShortCutForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BusinessSkinForm, ComCtrls, bsSkinCtrls, bsSkinBoxCtrls, Menus;

type
  TSetShortCut = class(TForm)
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    bsSkinGroupBox1: TbsSkinGroupBox;
    ListBox1: TbsSkinListBox;
    bsSkinGroupBox2: TbsSkinGroupBox;
    bsSkinButton1: TbsSkinButton;
    bsSkinButton2: TbsSkinButton;
    bsSkinButton3: TbsSkinButton;
    HotKey1: THotKey;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    procedure bsSkinButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox1ListBoxClick(Sender: TObject);
    procedure bsSkinButton1Click(Sender: TObject);
    procedure bsSkinButton2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SetShortCut: TSetShortCut;
  MyShortCut2:array[1..18]of TShortCut;
  MyShortCutB:array[1..18]of boolean;

implementation
uses MainForm,publicvar;

{$R *.dfm}

procedure TSetShortCut.bsSkinButton1Click(Sender: TObject);
const
  s:array[1..18]of string=('老板键','打开文件','全屏','查找','复制','标记','下一页','前一页','下一行','前一行','自动翻页','语音开始','语音停止','语音速度+','语音速度-','语音音量+','语音音量-','下一段');
begin
  MyShortCut2[ListBox1.ItemIndex+1]:=HotKey1.HotKey;
  MyShortCutB[ListBox1.ItemIndex+1]:=True;
  ListBox1.Items[ListBox1.ItemIndex]:=format('%-30s%s',[s[ListBox1.ItemIndex+1],ShortCutToText(MyShortCut2[ListBox1.ItemIndex+1])]);
end;

procedure TSetShortCut.bsSkinButton2Click(Sender: TObject);
var
  i:integer;
begin
  with Reader do
  for i := 1 to 18 do
  begin
    MyShortCut[i]:=MyShortCut2[i];
    case i of
      1:begin
        bosskey := GlobalAddAtom('MyHotKey') - $C000;
        ShortCutToKey(MyShortCut[i], Key, T);
        Shift := ShiftStateToWord(T);
        RegisterHotKey(Handle, bosskey, Shift, Key);
      end;
      2:OpenFile1.ShortCut:=MyShortCut[i];
      3:FullScreen1.ShortCut:=MyShortCut[i];
      4:Find1.ShortCut:=MyShortCut[i];
      5:Copy1.ShortCut:=MyShortCut[i];
      6:Markit1.ShortCut:=MyShortCut[i];
      7:NextPage1.ShortCut:=MyShortCut[i];
      8:PrePage1.ShortCut:=MyShortCut[i];
      9:nextline1.ShortCut:=MyShortCut[i];
      10:preline1.ShortCut:=MyShortCut[i];
      11:AutoCrossPage1.ShortCut:=MyShortCut[i];
      12:VoiceSpeak1.ShortCut:=MyShortCut[i];
      13:VoiceStop1.ShortCut:=MyShortCut[i];
      14:VoiceSpeedAdd.ShortCut:=MyShortCut[i];
      15:VoiceSpeedMul.ShortCut:=MyShortCut[i];
      16:VoiceVolAdd.ShortCut:=MyShortCut[i];
      17:VoiceVolMul.ShortCut:=MyShortCut[i];
      18:NextParagraph.ShortCut:=MyShortCut[i];
    end;
  end;
  for i := 1 to 18 do if MyShortCutB[i] then
  try
    Reader.Query1.Close;
    Reader.Query1.SQL.Text:='select * from shortcutinfo where id='+inttostr(i);
    Reader.Query1.Open;
    if not Reader.Query1.IsEmpty then
    begin
      Reader.Query1.Close;
      Reader.Query1.SQL.Text:='update shortcutinfo SET ShortCut="'+inttostr(MyShortCut[i])+'" where id='+inttostr(i);
      Reader.Query1.Open;
    end else begin
      Reader.Query1.Close;
      Reader.Query1.SQL.Text:='insert into shortcutinfo(id,ShortCut) values("'+inttostr(i)+'","'+inttostr(MyShortCut[i])+'")';
      Reader.Query1.Open;
    end;
  except

  end;
  Close;
end;

procedure TSetShortCut.bsSkinButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TSetShortCut.FormCreate(Sender: TObject);
const
  s:array[1..18]of string=('老板键','打开文件','全屏','查找','复制','标记','下一页','前一页','下一行','前一行','自动翻页','语音开始','语音停止','语音速度+','语音速度-','语音音量+','语音音量-','下一段');
var
  i:integer;
begin
  Listbox1.Clear;
  for i := 1 to 18 do
  begin
    Listbox1.Items.Add(format('%-30s%s',[s[i],ShortCutToText(MyShortCut[i])]));
    MyShortCut2[i]:=MyShortCut[i];
    MyShortCutB[i]:=false;
  end;
  ListBox1.ItemIndex:=0;
  HotKey1.HotKey:=MyShortCut2[1];
end;

procedure TSetShortCut.ListBox1ListBoxClick(Sender: TObject);
begin
  HotKey1.HotKey:=MyShortCut[ListBox1.ItemIndex+1];
end;

procedure TSetShortCut.N1Click(Sender: TObject);
begin
  HotKey1.HotKey:=TexttoShortCut('Space');
end;

procedure TSetShortCut.N2Click(Sender: TObject);
begin
  HotKey1.HotKey:=TexttoShortCut('Enter');
end;

end.
