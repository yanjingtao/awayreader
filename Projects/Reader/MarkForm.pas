unit MarkForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BusinessSkinForm, bsSkinCtrls, bsSkinBoxCtrls, Menus, bsSkinMenus,
  StdCtrls;

type
  TMarkF = class(TForm)
    bsBusinessSkinForm1: TbsBusinessSkinForm;
    MarkLists: TbsSkinListBox;
    bsSkinPopupMenu1: TbsSkinPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    status: TbsSkinStdLabel;
    procedure FormCreate(Sender: TObject);
    procedure MarkListsListBoxDblClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure bsSkinPopupMenu1Popup(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MarkListsListBoxClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MarkF: TMarkF;

implementation
uses MainForm,publicvar;

{$R *.dfm}

procedure TMarkF.bsSkinPopupMenu1Popup(Sender: TObject);
begin
  if MarkLists.ItemIndex>-1 then
    N1.Enabled:=True
  else N1.Enabled:=False;
end;

procedure TMarkF.FormCreate(Sender: TObject);
begin
  MarkLists.Width:=MarkLists.Glyph.Canvas.TextWidth('啊')*20;
end;

procedure TMarkF.FormShow(Sender: TObject);
begin
  status.Caption:='共标记'+inttostr(MarkLists.Items.Count)+'个';
end;

procedure TMarkF.MarkListsListBoxClick(Sender: TObject);
begin
  if MarkLists.ItemIndex>=0 then status.Caption:='共标记'+inttostr(MarkLists.Items.Count)+'个    此标记位置:'+(BookMarks[MarkLists.ItemIndex]);
end;

procedure TMarkF.MarkListsListBoxDblClick(Sender: TObject);
begin
  if MarkLists.ItemIndex>-1 then Reader.Read.Pos:=strtoint(BookMarks[MarkLists.ItemIndex]);
end;

procedure TMarkF.N1Click(Sender: TObject);
const
  s:array[1..2]of string=('选定的','全部');
begin
  if Reader.RMessage.MessageDlg('你确定要删除'+s[(Sender as TMenuItem).Tag]+'记录吗？',mtConfirmation,[mbYES,MbNO],0)=mrYES then
    case (Sender as TMenuItem).Tag of
      1:if MarkLists.ItemIndex>-1 then Reader.DeleteMark(MarkLists.ItemIndex);
      2:Reader.DeleteAllMarks;
    end;
  status.Caption:='共标记'+inttostr(MarkLists.Items.Count)+'个';
end;

end.
