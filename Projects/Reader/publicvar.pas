unit publicvar;

interface

uses Graphics,Windows,Classes,VBScript_RegExp_55_TLB,ShlObj;

type

Sets=record
  Font:TFont;       //字体
  BgType:Boolean;      //背景类型
  BgFile:string;    //背景图片
  BgMode:Integer;   //背景图片显示方式
  Color:TColor;     //背景颜色
  Memory:Boolean;   //自动清理内存
  AutoFullScreen:Boolean;               //自动全屏
  AutoSaveMark:Boolean;                 //自动保存书签
  AutoCrossPage:Boolean;                //自动翻页
  CrossType:byte;                       //翻页类型
  CrossTime:integer;                    //翻页时间
  RowSpacing:integer;                   //行距
  ListSpacing:integer;                  //列宽
  FileID:integer;                       //文件在数据库的编号
  LastCloseFile:String;                 //上次关闭的文件
  SkinFile:String;                      //皮肤文件
  ScrollBar:Boolean;                    //右侧滚动条
  VoiceIndex:Integer;                   //语音选项
  VoiceRate:Integer;                    //音速
  VoiceVol:Integer;                     //音量
  AwokeHour:Integer;                    //小时提醒
  UseOneFont:Boolean;                   //使用统一字体
  OnyOne:Boolean;                       //只允许一个实例
  DetectDir:String;                     //监视目录
  MarginLeft:integer;                   //左边距
  MarginRight:integer;                  //右边距
  MarginTop:integer;                    //上边距
  MarginBottom:integer;                 //下边距
end;

FindRecord=record
  machs: IMatchCollection;
  submatch: ISubMatches;
  Findpos: Integer;
  Count: Integer;
end;

function booleantostr(b:boolean):string;
function GetUserPath(FID: Integer): string;

const
  Reader_version = ' V3.3.0 Final';
  VersionID=4;
  Ansi=0;
  UTF8=1;
  Unicode=2;
  ShortCutDome:array[1..18]of TShortCut=(16465,16463,32781,16454,16451,16450,0,0,0,0,0,16464,16467,0,0,0,0,0);
  //1..18:老板键,打开文件,全屏,查找,复制,标记,下一页,前一页,下一行,前一行,自动翻页,语音开始,语音停止,语音速度+,语音速度-,语音音量+,语音音量-,下一段
var
  SystemSets:Sets;
  FindObj:FindRecord;
  BookMarks:TStringList;
  MyShortCut:array[1..18]of TShortCut;
  LastFileName:String;
  Codes:integer;
implementation

function booleantostr(b:boolean):string;
begin
  if b then
    Result:='1'
  else Result:='0';
end;

{获取常用路径的函数}
function GetUserPath(FID: Integer): string;
var
  pidl: PItemIDList;
  path: array[0..MAX_PATH] of Char;
begin
  SHGetSpecialFolderLocation(0, FID, pidl);
  SHGetPathFromIDList(pidl, path);
  Result := path;
end;

end.
