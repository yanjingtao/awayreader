program AwayReader;

uses
  Forms,
  MainForm in 'MainForm.pas' {Reader},
  publicvar in 'publicvar.pas',
  ABOUT in 'ABOUT.pas' {AboutBox},
  md5 in 'md5.pas',
  MarkForm in 'MarkForm.pas' {MarkF},
  FileManagerForm in 'FileManagerForm.pas' {FileManager};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := '�ζ��������Ķ���';
  Application.CreateForm(TReader, Reader);
  Application.CreateForm(TAboutBox, AboutBox);
  Application.CreateForm(TMarkF, MarkF);
  Application.CreateForm(TFileManager, FileManager);
  Application.Run;
end.
