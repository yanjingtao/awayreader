For Delphi 5, 6, 7, 2005, 2006, 2007, 2009:

 - open bsfd*.dpk file
 - install package

For C++Builder 5, 6:

 - open bsfcb*.bpk file
 - install package

For C++Builder 2006[7,9]:

 - open bsfcb2006[7,9].bdsproj file
 - install package

See bsfsb.zip with SkinBuilder sources.

