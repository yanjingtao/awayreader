#SZ Makefile Generator 1.0.0 for BCC 5.5.1
#Author Sasa Zeman, www.szutils.net

#This simple generator is based on 
#Michel Leunen's MFGen.exe, http://www.leunen.com/

BPATH=$(MAKEDIR)
INCLUDEPATH=$(BPATH)\..\include
LIBPATH=$(BPATH)\..\lib

CC=$(BPATH)\bcc32
RC=$(BPATH)\brcc32
LINK=$(BPATH)\ilink32

SYSOBJS=c0x32.obj
SYSLIBS=cw32.lib import32.lib
CFLAGS=-pc -tWC -w- -O2 -tWM- -H- -DSQLITE_CORE=1 -DSQLITE_ENABLE_FTS3=1  -I$(INCLUDEPATH) 
LFLAGS=-Tpe -ap -x -Gn -L$(LIBPATH)
RFLAGS=-32 -i$(INCLUDEPATH)

PROJECT=sqlite3.exe
RES=
LIBS=
DEFS=
OBJS= sqlite3005009-cdecl.obj shell.obj
     
.autodepend

all: $(PROJECT)
$(PROJECT): $(OBJS) $(RES)
   $(LINK) $(LFLAGS) $(OBJS) $(SYSOBJS),$(PROJECT),,$(SYSLIBS) $(LIBS),$(DEFS),$(RES)

.cpp.obj:
	$(CC) -c $(CFLAGS) $<

.cc.obj:
	$(CC) -c $(CFLAGS) $<

.c.obj:
	$(CC) -c $(CFLAGS) $<

.rc.res:
	$(RC) $(RFLAGS) $<
